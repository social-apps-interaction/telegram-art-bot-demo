package telegram;

import org.junit.Test;

import static org.junit.Assert.*;

public class CommandTest {

    @Test
    public void getEnum() throws Exception {
        Command result = Command.getEnum("/start");
        assertEquals(Command.START,result);
    }

}