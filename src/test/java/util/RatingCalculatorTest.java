package util;

import dao.GameDao;
import dao.entity.Game;
import dao.entity.Round;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

public class RatingCalculatorTest {

    @Mock
    GameDao gameDao;

    @InjectMocks
    RatingCalculator ratingCalculator;

    @Test
    public void getUserCorrectAnswersRatingPosition() throws Exception {
        MockitoAnnotations.initMocks(this);
        int userA = 154;
        int userB = 32;
        Mockito.when(gameDao.getUsersIds()).thenReturn(Arrays.asList(userB, 154));
        Mockito.when(gameDao.getUserGames(userB)).thenReturn(Arrays.asList(new Game()));
        Game game = new Game();
        Round round = new Round();
        round.setIsWon(true);
        game.setRounds(Arrays.asList(round));
        Mockito.when(gameDao.getUserGames(154)).thenReturn(Arrays.asList(game));


        Integer userCorrectAnswersRatingPosition = ratingCalculator.getUserCorrectAnswersRatingPosition(userA);
        Integer userCorrectAnswersRatingPositionB = ratingCalculator.getUserCorrectAnswersRatingPosition(userB);

        assert userCorrectAnswersRatingPosition == 1;
        assert userCorrectAnswersRatingPositionB == 2;
    }

}