package dao.impl;

import dao.PictureDao;
import dao.entity.Game;
import dao.entity.Picture;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * Created by Ilya on 22.09.2017.
 */
public class GameDaoImplTest extends AbstractDbTest {

    private GameDaoImpl gameDao = new GameDaoImpl();
    private PictureDao pictureDao = new PictureDaoImpl();

    @Test
    public void save() throws Exception {
        Picture randomPicture = pictureDao.getRandomPicture();
        Game game = new Game();
        int userId = 555;
        game.setUserId(userId);
        game.setPicture(randomPicture);

        gameDao.save(game);

        Optional<Game> gameOptional = gameDao.get(game.getId());

        assertTrue(gameOptional.isPresent());
        Game gameFromDB = gameOptional.get();
        assertEquals((long) userId, (long) gameFromDB.getUserId());
    }

    @Test
    public void getCurrentGame() throws Exception {
        Picture randomPicture = pictureDao.getRandomPicture();
        Game game = new Game();
        int userId = 555;
        game.setUserId(userId);
        game.setPicture(randomPicture);
        game.setIsFinished(false);

        gameDao.save(game);

        Optional<Game> currentGame = gameDao.getCurrentGame(userId);
        assertTrue(currentGame.isPresent());

        game.setIsFinished(true);
        gameDao.save(game);

        currentGame = gameDao.getCurrentGame(userId);
        assertFalse(currentGame.isPresent());
    }

    @Test
    public void getUsersIds() throws Exception {
        List<Integer> usersIds = gameDao.getUsersIds();
        assertFalse(usersIds.isEmpty());
        assertTrue(usersIds.contains(333));
    }

}