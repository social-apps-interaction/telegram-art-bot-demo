package dao.impl;

import dao.GameDao;
import dao.RoundDao;
import dao.entity.Game;
import dao.entity.Picture;
import dao.entity.Question;
import dao.entity.Round;
import org.junit.Test;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

public class PictureDaoImplTest extends AbstractDbTest {

    PictureDaoImpl pictureDao = new PictureDaoImpl();
    RoundDao roundDao = new RoundDaoImpl();

    @Test
    public void getRandomNotPlayedPicture() throws Exception {
        clearDataBase();

        Picture pictureA = new Picture();
        Question questionA = new Question();
        pictureA.setQuestions(Arrays.asList(questionA));
        questionA.setPicture(pictureA);
        pictureDao.save(pictureA);

        int userId = 341;
        GameDao gameDao = new GameDaoImpl();
        Game game = new Game();
        game.setUserId(userId);
        game.setPicture(pictureDao.getRandomPicture());
        gameDao.save(game);

        Picture pictureB = new Picture();
        Question questionB = new Question();
        pictureB.setQuestions(Arrays.asList(questionB));
        questionB.setPicture(pictureB);
        pictureDao.save(pictureB);

        Round round = new Round();
        round.setQuestion(pictureA.getQuestions().get(0));

        round.setGame(game);
        roundDao.saveRound(round);

        Picture randomNotPlayedPicture = pictureDao.getRandomNotPlayedPicture(userId);
        assertEquals(pictureB.getId(), randomNotPlayedPicture.getId());
    }

    @Test
    public void getRandomNotPlayedPictureWhenNewUser() throws Exception {
        clearDataBase();
        Picture pictureA = new Picture();
        Question questionA = new Question();
        pictureA.setQuestions(Arrays.asList(questionA));
        questionA.setPicture(pictureA);
        pictureDao.save(pictureA);

        Picture randomNotPlayedPicture = pictureDao.getRandomNotPlayedPicture(3125);
        assertEquals(pictureA.getId(), randomNotPlayedPicture.getId());
    }

    @Test
    public void getPicture() throws Exception {
        Optional<Picture> pictureOptional = pictureDao.getPicture(1000);
        assertTrue(pictureOptional.isPresent());

        Picture picture = pictureOptional.get();
        assertEquals("Caption", picture.getCaption());
    }

    @Test
    public void getNotExistedPicture() throws Exception {
        Optional<Picture> pictureOptional = pictureDao.getPicture(-500);
        assertFalse(pictureOptional.isPresent());
    }

    @Test
    public void getRandomPicture() throws Exception {
        Picture randomPicture = pictureDao.getRandomPicture();
        assertNotNull(randomPicture);
    }

    @Test
    public void save() throws Exception {
        List<Question> questions = new ArrayList<>();
        Question question = new Question();
        question.setQuestion("Сколько человек изображено на картине?");
        question.setAnswer("7");
        question.setIncorrectAnswerA("8");
        question.setIncorrectAnswerB("5");
        questions.add(question);
        Picture picture = new Picture("http://decordepo.ru/katalog/collection/pictures/45571.jpg", "Святой Иероним поддерживает казнённых", questions, Instant.now());
        question.setPicture(picture);

        pictureDao.save(picture);

        Optional<Picture> pictureFromDb = pictureDao.getPicture(picture.getId());

        assertEquals(1, pictureFromDb.get().getQuestions().size());
    }

    @Test
    public void getAllPictures()throws Exception{
        List<Picture> allPictures = pictureDao.getAllPictures();
        assertNotNull(allPictures);
    }

}