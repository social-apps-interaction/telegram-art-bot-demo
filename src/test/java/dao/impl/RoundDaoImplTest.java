package dao.impl;

import dao.GameDao;
import dao.PictureDao;
import dao.entity.Game;
import dao.entity.Question;
import dao.entity.Round;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by Ilya on 19.09.2017.
 */
public class RoundDaoImplTest extends AbstractDbTest {

    RoundDaoImpl roundDao = new RoundDaoImpl();
    PictureDao pictureDao = new PictureDaoImpl();

    @Test
    public void getGameById() throws Exception {
        Optional<Round> gameOptional = roundDao.getRound(3000);
        assertTrue(gameOptional.isPresent());

        Round round = gameOptional.get();

        assertEquals(2000, round.getQuestion().getId().longValue());
    }

    @Test
    public void saveGame() throws Exception {
        GameDao gameDao = new GameDaoImpl();
        Game game = new Game();
        game.setPicture(pictureDao.getRandomPicture());
        game.setUserId(555);
        gameDao.save(game);

        Round round = new Round();
        Question question = pictureDao.getRandomPicture().getQuestions().get(0);
        round.setQuestion(question);
        round.setGame(game);

        this.roundDao.saveRound(round);

        Optional<Round> savedGameOptional = this.roundDao.getRound(round.getId());
        assertTrue(savedGameOptional.isPresent());
        Round savedRound = savedGameOptional.get();
        assertEquals(question.getAnswer(), savedRound.getQuestion().getAnswer());
    }

}