package dao.impl;

import dao.UserDao;
import dao.entity.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

public class UserDaoImplTest extends AbstractDbTest {

    UserDao userDao;

    @Before
    public void setUp() throws Exception {
        userDao = new UserDaoImpl();
    }

    @Test
    public void saveAndGet() throws Exception {
        User user = new User();
        String firstName = "Васёк";

        user.setId(55);
        user.setFirstName(firstName);
        userDao.saveOrUpdate(user);

        Optional<User> userOptional = userDao.getUser(55);
        Assert.assertTrue(userOptional.isPresent());
        Assert.assertEquals(firstName, userOptional.get().getFirstName());
    }

}