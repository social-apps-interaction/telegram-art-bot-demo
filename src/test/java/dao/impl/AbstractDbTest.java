package dao.impl;

import configuration.MainConfig;
import dao.DatabaseManager;
import org.aeonbits.owner.ConfigFactory;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.Before;
import org.junit.BeforeClass;

public abstract class AbstractDbTest {

    MainConfig mainConfig = ConfigFactory.create(MainConfig.class);

    @Before
    public void populateDB() {
        assert mainConfig.dbName().contains("test");
        assert mainConfig.dbUsername().contains("test");

        clearDataBase();
        standardDatabaseInserting();
    }

    @BeforeClass
    public static void liquibaseMigration() throws Exception {
        DatabaseManager.createOrUpdateDatabaseSchema();
    }

    protected static void clearDataBase() {
        executeUpdateSqlQuery("delete from round");
        executeUpdateSqlQuery("delete from game");
        executeUpdateSqlQuery("delete from question");
        executeUpdateSqlQuery("delete from picture");
        executeUpdateSqlQuery("delete from handled_update");
    }

    protected static void standardDatabaseInserting() {
        executeUpdateSqlQuery("insert into picture values (1000,'Caption',CURRENT_TIMESTAMP,'post_img/2016/03/21/6/1458550072121813477.jpg')");
        executeUpdateSqlQuery("insert into question values (2000,'4','3','5','2+2',1000)");
        executeUpdateSqlQuery("insert into game (id,is_finished,upd_date,user_id,picture_id ) values (10,true,CURRENT_TIMESTAMP,333,1000)");
        executeUpdateSqlQuery("insert into round (id,is_won,game_id,question_id) values (3000,null,10,2000)");
    }

    protected static void executeUpdateSqlQuery(String sqlQuery) {
        Session session = DatabaseManager.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.createNativeQuery(sqlQuery).executeUpdate();
            transaction.commit();
        } finally {
            session.close();
        }
    }

}
