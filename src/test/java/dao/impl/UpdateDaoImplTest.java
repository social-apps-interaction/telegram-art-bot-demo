package dao.impl;

import dao.entity.HandledUpdate;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Ilya on 18.09.2017.
 */
public class UpdateDaoImplTest extends AbstractDbTest {

    UpdateDaoImpl updateDao = new UpdateDaoImpl();

    @Test
    public void saveAndGet() throws Exception {
        HandledUpdate updateEntity = new HandledUpdate();
        updateEntity.setUpdateId(55);
        updateDao.save(updateEntity);

        assertTrue(updateDao.getUpdate(55).isPresent());
    }

}