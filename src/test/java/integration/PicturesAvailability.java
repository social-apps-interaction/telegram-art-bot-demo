package integration;

import configuration.MainConfig;
import dao.PictureDao;
import dao.entity.Picture;
import dao.impl.PictureDaoImpl;
import org.aeonbits.owner.ConfigFactory;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static io.restassured.RestAssured.given;

public class PicturesAvailability {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Test
    public void test() throws Exception {
        PictureDao pictureDao = new PictureDaoImpl();
        List<Picture> pictures = pictureDao.getAllPictures();
        pictures.parallelStream().forEach(p -> checkPictureAvailability(p));
    }

    private void checkPictureAvailability(Picture picture) {
        MainConfig mainConfig = ConfigFactory.create(MainConfig.class);
        String imageHostingUrl = mainConfig.imageHostingUrl();

        String url = imageHostingUrl + picture.getFileName();
        logger.info("Request url {} for picture {}", url, picture);
        given().get(url).then().statusCode(200);
    }

}
