﻿insert into picture values (10101,'В. А. Серов. Девочка с персиками. 1887 г.',CURRENT_TIMESTAMP,'Serov_V.-Devochka_s_persikami.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','3','5','Сколько персиков на картине?',10101);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа внизу','Слева вверху','Слева внизу','Где на картине расположена подпись автора?',10101);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На стене','На столе','На подоконнике','Где художник расположил блюдо?',10101);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На зрителя','В окно','В книгу','Куда смотрит героиня картины?',10101);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Розовая','Голубая','Лиловая','Какого цвета кофта у героини?',10101);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На окнах нет штор','Желтые','Зеленые','Какого цвета шторы на окнах?',10101);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Нож','Вилка','Иголка для шитья','Какой колющий предмет лежит на столе?',10101);

insert into picture values (10102,'И. Е. Репин. Запорожцы. 1880 г.',CURRENT_TIMESTAMP,'Repin_I.-Zaporozhcy.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Собака','Кот','Козел','Какое животное присутствует на картине?',10102);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько казаков с голым торсом изображено на картине?',10102);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Карты','Кости','Домино','Какая настольная игра, лежащая на столе, попала в кадр?',10102);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Домра','Барабан','Дудка','Какой музыкальный инструмент лежит на коленях у одного из героев?',10102);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Желтый','Синий','Какого цвета кафтан у смеющегося усача, упершего руки в бока?',10102);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На бочке','На лавке','На пне','На чем полулежит козак в белой рубахе, расположившийся спиной к зрителю?',10102);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голова','Плечо','Живот','Что перебинтовано у раненного персонажа, стоящего слева от писаря?',10102);

insert into picture values (10103,'Н. К. Пимоненко. Брод. 1901 г.',CURRENT_TIMESTAMP,'Pimonenko_N.-Brod.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько детей на картине погоняют скот?',10103);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','2','3','Сколько коров на картине?',10103);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Гуси','Куры','Сороки','Какие птицы наблюдают за процессией?',10103);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По воде','По траве','По песку','Дети шагают...',10103);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Синий','Желтый','Какого цвета сарафан у девочки на картине?',10103);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Блондинка','Брюнетка','Рыжая','Какой цвет волос у девочки на картине?',10103);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В правой','В левой','У него нет хлыста','В какой руке мальчик держит хлыст для погона скота?',10103);

insert into picture values (10104,'И. А. Пелевин. Первенец. 1888 г.',CURRENT_TIMESTAMP,'Pelevin_I.-Pervenec.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В детской кроватке','На подоконнике','В корзине','Где расположился котенок?',10104);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','Прямо за спиной','Где на картине расположено окно?',10104);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Играет с клубком','Умывается','Спит','Что делает котенок?',10104);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Полотенце','Подкова','Икона','Что висит у окна?',10104);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько ярусов у полки с посудой?',10104);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Улыбается','Плачет','Хмурится','Мать на картине...',10104);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа внизу','Слева внизу','Слева вверху','Где на картине подпись автора?',10104);

insert into picture values (10105,'М. В. Нестеров. Портрет академика физиолога И. П. Павлова. 1935 г.',CURRENT_TIMESTAMP,'Nesterov_M.-Portret_akademika_fiziologa_I._P._Pavlova.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белого','Синего','Красного','Какого цвета цветы в вазе перед мужчиной?',10105);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Усы и борода','Усы','Борода','Какая отличительная черта во внешности мужчины?',10105);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Три','Одна','Две','Сколько пуговиц на рукаве пиджака?',10105);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красного и зеленого','Красного и синего','Синего и зеленого','Какого цвета крыши домов на заднем плане?',10105);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сжимает кулаки','Читает газету','Пишет письмо','Что делает мужчина на картине?',10105);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лиловая','Желтая','Стол не застелен','Какого цвета скатерть на столе?',10105);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Часы','Галстук','Очки','Какой аксессуар отсутствует у мужчины?',10105);

insert into picture values (10106,'В. М. Максимов. Примерка ризы. 1878 г.',CURRENT_TIMESTAMP,'Maksimov_V.-Primerka_rizy.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько людей на картине?',10106);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В левой части','В правой части','По середине','Где на картине расположены иконы?',10106);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белый','Черный','Зеленый','Какого цвета чепец на голове у пожилой женщины?',10106);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ножницы','Иголку','Подол платья','Что портниха держит в правой руке?',10106);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько стульев на картине?',10106);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько зажженных свечей на картине?',10106);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтый и зеленый','Желтый и красный','Красный и синий','Каковы основные цвета примеряемого наряда?',10106);

insert into picture values (10107,'К. Е. Маковский. Жница. 1871 г.',CURRENT_TIMESTAMP,'Makovskij_K.-ZHnica.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ясное без облаков','Ясное с облаками','Пасмурное','Какое небо на картине?',10107);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кормит младенца','Одевает младенца','Баюкает младенца','Что делает женщина на картине?',10107);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько детей на картине?',10107);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Синий','Зеленый','Какого цвета платок у главной героини?',10107);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько человек собирают снопы на заднем плане?',10107);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Правый средний','Правый безымянный','Левый безымянный','На каком пальце кольцо у главной героини?',10107);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Оранжевая','Белая','Голубая','Какого цвета миска?',10107);

insert into picture values (10108,'В. Е. Маковский. Пастушки. 1903 г.',CURRENT_TIMESTAMP,'Makovskij_V.-Pastushki.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько детей на картине?',10108);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','1','2','Сколько коров на картине?',10108);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Ее там нет','Где на картине расположилась черная корова?',10108);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','Ни одного','Сколько детей на картине с непокрытой головой?',10108);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На опушке леса','На лугу','У реки','Где пасутся коровы?',10108);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ясный день','Пасмурный день','Дождливый день','Какая погода на картине?',10108);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На пне','На бревне','На траве','На чем сидит один из ребят?',10108);

insert into picture values (10109,'Н. Д. Кузнецов. В отпуску. 1882 г.',CURRENT_TIMESTAMP,'Kuznecov_N.-V_otpusku.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько людей на картине верхом?',10109);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько собак на картине?',10109);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('У одной','Все с ошейниками','Все без ошейников','У скольких собак нет ошейника?',10109);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Его там нет','В левой','В правой','В какой части картины расположено озеро?',10109);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Прикуривает','Машет рукой','Заряжает пистолет','Что делает всадник на белом коне?',10109);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Усы','Борода','Усы и борода','Отличительная черта внешности всадника на черном коне?',10109);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Фуражка','Папаха','Голова не покрыта','Что на голове у всадника на черном коне?',10109);

insert into picture values (10110,'П. И. Коровин. Крестины. 1896 г.',CURRENT_TIMESTAMP,'Korovin_P.-Krestiny.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('10','8','12','Сколько человек изображено на картине?',10110);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Ее там нет','В какой части картины расположена дверь?',10110);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На табурете','На столе','На алтаре','Где лежат незажженые свечки?',10110);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','Никто не крестится','Сколько человек на картине крестятся?',10110);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вверху слева','Вверху справа','Вверху по центру','В какой части картины расположена керосиновая лампа',10110);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Она босая','Лапти','Сандалии','Во что обута маленькая девочка в платке, сложившая на груди руки?',10110);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько зажженных свечей на картине?',10110);


insert into picture values (10111,'И. К. Айвазовский. Вид Константинополя и Босфора. 1856 г.',CURRENT_TIMESTAMP,'Ajvazovskij_I.-Vid_Konstantinopolya_i_Bosfora.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Низко','Высоко','За ним','Как расположено солнце по отношению к горизонту?',10111);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','По центру','Где на картине расположена мечеть?',10111);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','3','4','Сколько видимых башен у мечети?',10111);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','2','3','Сколько лодок изображено на переднем плане картины?',10111);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синее','Серое','Зеленое','Какого цвета море на картине?',10111);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кальян','Трубку','Папиросу','Что курит турок, сидящий у причала?',10111);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белые','Зеленые','Оранжевые','Какого цвета стены мечети?',10111);

insert into picture values (10112,'И. К. Айвазовский. Зимний караван через украинскую степь. 1857 г.',CURRENT_TIMESTAMP,'Ajvazovskij_K.-Zimnij_karavan_cherez_ukrainskuyu_step.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','По центру','Где на картине расположен дом?',10112);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько видимых окон у дома?',10112);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько людей беседеют перед домом?',10112);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красное','Белое','Синее','Какого цвета пальто у мужчины, стоящего спиной к зрителю?',10112);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Сверху','Откуда светит солнце на картине?',10112);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Стая птиц','Свора собак','Убегающая кошка','Слева от каравана изображена...',10112);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Волы','Лошади','Верблюды','Какие животные тянут караван?',10112);

insert into picture values (10113,'А. Е. Архипов. По реке Оке. 1889 г.',CURRENT_TIMESTAMP,'Arhipov_A.-Po_reke_Oke.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтый','Синий','Белый','Какого цвета платок у старухи, сидящей на правой стороне лодки?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Подпирает голову','Поправляет платок','Машет','Что делает левой рукой женщина в красном платке?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Красная','Зеленая','Какого цвета рубаха у мужчины с веслом?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Усы и борода','Усы','Борода','Какая отличительная особенность во внешности мужчины с веслом?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','По середине','В какой части картины расположен парусник?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько человек на картине идут вброд?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','Ни одного','Сколько человек стоит на носу лодки?',10113);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько кувшинов изображено на картине?',10113);

insert into picture values (10114,'П. В. Басин. Портрет Ольги Владимировны Басиной, жены художника. Между 1838 и 1841 г.',CURRENT_TIMESTAMP,'Basin_P.-Portret_Olgi_Vladimirovny_Basinoj_zheny_hudozhnika.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синяя','Желтая','Зеленая','Какого цвета столешница?',10114);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На зрителя','В пол','В окно','Куда смотрит героиня картины?',10114);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бордовое','Розовое','Белое','Какого цвета платье у героини?',10114);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На левой','На правой','У нее нет колец','На какой руке у героини кольцо?',10114);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','По центру','Где на картине расположено окно?',10114);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ваза с цветами','Корзина с фруктами','Книга','Что изображено на столе возле героини?',10114);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Брюнетка','Блондинка','Рыжая','Девушка, изображенная на картине - ...',10114);

insert into picture values (10115,'Н. А. Богатов. Пасечник. 1875 г.',CURRENT_TIMESTAMP,'Bogatov_N.-Pasechnik.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Книгу','Яблоко','Курительную трубку','Что пасечник держит в руках?',10115);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Он босой','Сапоги','Лапти','Что надето на ноги старика?',10115);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На траве','На лавке','На бревнах','На чем сидит старик?',10115);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Красная','Синяя','Какого цвета рубаха у старика?',10115);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('6','4','8','Сколько ульев изображено на картине?',10115);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ясный день','Пасмурный день','Дождливый день','Какая погода на картине?',10115);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голова не покрыта','Соломенная шляпа','Фуражка','Какой головной убор на голове у пасечника?',10115);

insert into picture values (10116,'Е. И. Ботман. Портрет Александра II. 1856 г.',CURRENT_TIMESTAMP,'Botman_E.-Portret_Aleksandra_II.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красные','Белые','Черные','Какого цвета штаны у государя?',10116);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Усы','Борода','Усы и борода','Какая отличительная особенность во внешности государя?',10116);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вправо','Влево','На зрителя','В какую сторону направлен на картине взгляд государя?',10116);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На берегу водоема','На опушке леса','На площади','Где стоит государь?',10116);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голубая','Красная','Желтая','Какого цвета лента на груди у государя?',10116);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Шпага','Пистолет','Государь безоружен','Какое оружие висит на поясе Александра II?',10116);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В левой руке','В правой руке','На голове','Головной убор государя изображен...',10116);

insert into picture values (10117,'К. П. Брюллов. Всадница. 1832 г.',CURRENT_TIMESTAMP,'Bryullov_K.-Vsadnica.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','3','4','Сколько людей изображено на картине?',10117);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько собак на картине?',10117);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черный','Белый','Серый','Каков окрас у коня главной героини?',10117);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Розовое','Желтое','Зеленое','Какого цвета платье у маленькой девочки на картине?',10117);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На зрителя','Назад','На собаку','Куда смотрит всадница?',10117);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На балконе','На лестнице','В траве','Где стоит маленькая девочка?',10117);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Там нет фонтана','Слева','Справа','Где на картине изображен фонтан?',10117);

insert into picture values (10118,'К. П. Брюллов. Последний день Помпеи. 1833 г.',CURRENT_TIMESTAMP,'Bryullov_K.-Poslednij_den_Pompei.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','1','2','Сколько падающих статуй на картине?',10118);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кони','Собаки','Крысы','Какие животные изображены на картине?',10118);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Брусчатка','Песок','Трава','Что под ногами у людей?',10118);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красно-черное','Серо-синее','Фиолетовое','Какого цвета небо на картине?',10118);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтая','Розовая','Голубая','Какого цвета накидка у женщины упавшей замертво?',10118);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На ступенях','На балконе','На крыше','На чем стоят люди в левой части картины?',10118);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','По центру','В какой части картины извергается вулкан?',10118);

insert into picture values (10119,'А. Г. Венецианов. На пашне. 1820 г.',CURRENT_TIMESTAMP,'Venecianov_A.-Na_pashne.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На ребенка','На зрителя','На небо','Куда смотрит девушка на картине?',10119);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кокошник','Косынка','Голова не покрыта','Что на голове у девушки?',10119);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','По центру','Где на картине изображен ребенок?',10119);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сидит','Стоит','Бежит','Что делает ребенок?',10119);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Зеленая','Красная','Какого цвета блуза главной героини?',10119);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Ее там нет','Где на картине изображена посадка молодых деревьев?',10119);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько лошадей главной героини тянут борону?',10119);

insert into picture values (10120,'К. Б. Вениг. Последние минуты Дмитрия Самозванца. 1879 г.',CURRENT_TIMESTAMP,'Venig_K.-Poslednie_minuty_Dmitriya_Samozvanca.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько людей на картине?',10120);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','По центру','Где на картине расположено окно?',10120);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтая','Красная','Зеленая','Какого цвета рубаха у Лжедмитрия?',10120);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сабля','Топор','Пистолет','Какое оружие висит на поясе у боярина?',10120);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Указывает в окно','Показывает кулак','Убегает','Что делает боярин?',10120);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Короткие','Длинные','Не видны','Волосы Лжедмитрия...?',10120);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Стул','Ваза с цветами','Статуя','Какой опрокинутый предмет интерьера изображен на картине?',10120);

insert into picture values (10300,'В. Е. Маковский. Девочка с гусями. 1875 г.',CURRENT_TIMESTAMP,'V._Makovskij-Devochka_s_gusyami.jpg');                      
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько всего гусей на картине?',10300);              
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По траве','По песку','По воде','Девушка идет...',10300);         
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('До локтя','До запястья','Платье без рукавов','Какой длины рукава на платье у девушки?',10300);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Браслет','Серьги','Бусы','Какого аксессуара нет на девушке?',10300);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Красный','Желтый','Какого цвета передник у девушки?',10300);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красного','Желтого','Голубого','Какого цвета платье у девушки?',10300); 
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Цветы','Яблоки','Хворост','Что несет девушка в переднике?',10300);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Венок','Косынка','Платок','Что на голове у девушки?',10300);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Она босая','Сандалии','Лапти','Что на ногах у девушки?',10300);

insert into picture values (10301,'В. Е. Маковский. Рыбачки. 1886. г.',CURRENT_TIMESTAMP,'Makovskij_V._E.-Rybachki.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красного','Зеленого','Желтого','Какого цвета рубаха у мальчика, расположенного на картине ближе к зрителю?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('У того, кто слева','У того, кто справа','У обоих','У кого из мальчиков есть головной убор?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Удочки','Рыба','Костер','Что присутствует на картине?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ни один','1','2','Сколько мальчиков улыбается?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Песок','Камни','Бревно','На чем сидят мальчики?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кувшин','Бутылка','Фляга','Какой сосуд изображен на картине?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','Снизу','В какой части картины раположен водоем?',10301);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лес','Луг','Деревня','Что изображено на заднем плане?',10301);

insert into picture values (10302,'В. М. Максимов. Все в прошлом. 1889 г.',CURRENT_TIMESTAMP,'Maksimov_V.-Vse_v_proshlom.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько окон видно на доме за спиной пожилых женщин?',10302);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вяжет','Вышивает','Заваривает чай','Что делает пожилая женщина в очках?',10302);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Собака','Кошка','Курица','Какое животное есть на картине?',10302);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Синяя','Зеленая','Какое цвета подушка под ногами у пожилой женщины в кресле?',10302);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На ступенях','На табуретке','На лавке','Где сидит пожилая женщина в очках?',10302);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сирень','Пионы','Розы','Что цветет за спиной у женщин?',10302);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В клетку','В полоску','В горошек','У пожилой женщины в очках передник...',10302);

insert into picture values (10303,'Г. Г. Манизер. Император Николай II с орденом Владимира. 1905 г. г.',CURRENT_TIMESTAMP,'Manizer_G._G.-Imperator_Nikolaj_II_s_ordenom_Vladimira.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красного','Желтого','Белого','Какого цвета пояс у Николая II?',10303);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько крестов на груди у императора?',10303);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Карие','Голубые','Зеленые','Какого цвета глаза у императора?',10303);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Очки','Головной убор','Кольцо','Что отсуствует у императора?',10303);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('За спиной','В кармане','За поясом','Левая рука императора...',10303);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Балюстрада','Фонтан','Ступени','Что находится за спиной у императора?',10303);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Зеленый','Черный','Какого цвета мундир у императора?',10303);

insert into picture values (10304,'Н. С. Матвеев. Против воли постриженная. 1887. г.',CURRENT_TIMESTAMP,'Matveev_N._S.-Protiv_voli_postrizhennaya.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','1','Сколько свечей видно на картине?',10304);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Зеленый','Красный','Какого цвета коврик под ногами монахини?',10304);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На полу','На подоконнике','В руках у монахини','Где ножницы?',10304);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','У скольких человек на картине не покрыта голова?',10304);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Церковь','Изба','Мост','Какое строение виднеется за окном?',10304);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красные','Синие','Белые','Какого цвета штаны у мужчины в цветастом кафтане?',10304);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','Посередине','В какой части картины изображено окно?',10304);

insert into picture values (10305,'М. В. Нестеров. Видение отроку Варфоломею. 1899 г.',CURRENT_TIMESTAMP,'Nesterov_M._V.-Videnie_otroku_Varfolomeyu.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько куполов у церкви на заднем плане?',10305);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белый','Зеленый','Голубой','Какой цвет рубахи у мальчика?',10305);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Осень','Весна','Зима','Какое время года изображено на картине?',10305);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Скрещены спереди','Затнуты за пояс','Спрятаны за спиной','В каком положении находятся руки у мальчика?',10305);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сапоги','Лапти','Он босой','Во что обут мальчик?',10305);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','Ее там нет','Где на картине изображена река?',10305);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Седая','Черная','Рыжая','Какого цвета борода у святого?',10305);

insert into picture values (10306,'Л. О. Пастернак. К родным. 1891 г.',CURRENT_TIMESTAMP,'Pasternak_L._O.-K_rodnym.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','1','2','Сколько человек на картине?',10306);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В окно','На зрителя','Вниз','Куда устремлен взгляд женщины слева?',10306);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черный','Красный','Белый','Какого цвета наряд у женщины справа?',10306);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Платок','Сумка','Газета','Что в руках у женщины справа?',10306);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Зеленый','Красный','Желтый','Какого цвета подлокотник сидения?',10306);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бусы','Серьги','Кольцо','Какой аксессуар есть на женщине возле окна?',10306);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вагон поезда','Карета','Зал ожидания','Место действия картины?',10306);

insert into picture values (10307,'И. Е. Репин.  Иов и его друзья. 1869 г.',CURRENT_TIMESTAMP,'Repin_I._E.-Iov_i_ego_druzya.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','Ни одной','Сколько женщины на картине?',10307);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Меч','Топор','Лук','Какое оружие у мужчины справа?',10307);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кувшин','Чаша','Ведро','Какой сосуд изображен на картине слева?',10307);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синего','Зеленого','Белого','Какого цвета накидка у мужчины справа?',10307);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сидит','Лежит','Стоит','Что делает мужчина с голым торсом?',10307);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лошадь','Верблюд','Собака','Какое животное не изображено на картине?',10307);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Гора','Пустыня','Дворец','Что изображено на заднем плане картины?',10307);

insert into picture values (10308,'И. Е. Репин. Не ждали. 1888 г.',CURRENT_TIMESTAMP,'Repin_I._E.-Ne_zhdali.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько картин висит на правой стене?',10308);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Женщина','Мужчина','Ребенок','Кто держит дверь открытой?',10308);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Мальчик и девочка','Девочка','Мальчик','Кто сидит за столом справа?',10308);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('7','5','9','Сколько всего человек изображено на картине?',10308);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пальто','Костюм','Униформа','Что надето на мужчине, который пришел?',10308);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черный','Красный','Синий','Какого цвета наряд на женщине, встающей из-за стола?',10308);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ваза','Ноты','Карта','Что из перечисленного отсутствует на картине?',10308);

insert into picture values (10309,'И. Е. Репин. Николай Мирликийский избавляет от смерти трех невинно осужденных. 1888 г. г.',CURRENT_TIMESTAMP,'Repin_I._E.-Nikolaj_Mirlikijskij_izbavlyaet_ot_smerti_trekh_nevinno_osuzhdennyh.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На груди','За спиной','У висков','Где находятся руки у мужчины, что на коленях?',10309);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Красный','Белый','Какого цвета нет на платке у палача?',10309);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На лезвии меча','На плече у осужденного','Поднята вверх','Где находится правая рука Николая Мирликийского?',10309);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Веревка','Кинжал','Крюк','Что висит за поясом у палача?',10309);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Копья','Луки и стрелы','Мечи','Какое оружие держит стража?',10309);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черные','Белые','Красные','Какого цвета кресты на одежде у Николая Мирликийского?',10309);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ладонь','Орел','Звезда','Что изображено на штандарте, виднеющемся на заднем плане?',10309);
