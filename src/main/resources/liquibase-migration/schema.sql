--liquibase formatted sql

--changeset Sobolev:1

SET client_encoding = 'UTF8';

--
-- TOC entry 186 (class 1259 OID 43395)
-- Name: game; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE game (
    id integer NOT NULL,
    is_finished boolean,
    picture_message_id integer,
    upd_date timestamp without time zone,
    user_id integer,
    picture_id integer NOT NULL
);

--
-- TOC entry 185 (class 1259 OID 43393)
-- Name: game_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE game_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2175 (class 0 OID 0)
-- Dependencies: 185
-- Name: game_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE game_id_seq OWNED BY game.id;


--
-- TOC entry 187 (class 1259 OID 43401)
-- Name: handled_update; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE handled_update (
    update_id integer NOT NULL,
    handling_date timestamp without time zone,
    is_handled boolean
);



--
-- TOC entry 189 (class 1259 OID 43408)
-- Name: picture; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE picture (
    id integer NOT NULL,
    caption character varying(255),
    cr_date timestamp without time zone,
    file_name character varying(255)
);


--
-- TOC entry 188 (class 1259 OID 43406)
-- Name: picture_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE picture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;




--
-- TOC entry 2176 (class 0 OID 0)
-- Dependencies: 188
-- Name: picture_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE picture_id_seq OWNED BY picture.id;


--
-- TOC entry 191 (class 1259 OID 43419)
-- Name: question; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE question (
    id integer NOT NULL,
    answer character varying(255),
    incorrect_answer_1 character varying(255),
    incorrect_answer_2 character varying(255),
    question character varying(1000),
    picture_id integer NOT NULL
);




--
-- TOC entry 190 (class 1259 OID 43417)
-- Name: question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2177 (class 0 OID 0)
-- Dependencies: 190
-- Name: question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE question_id_seq OWNED BY question.id;


--
-- TOC entry 193 (class 1259 OID 43430)
-- Name: round; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE round (
    id integer NOT NULL,
    is_current boolean,
    is_won boolean,
    user_answer character varying(255),
    game_id integer NOT NULL,
    question_id integer NOT NULL
);



--
-- TOC entry 192 (class 1259 OID 43428)
-- Name: round_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE round_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 2178 (class 0 OID 0)
-- Dependencies: 192
-- Name: round_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE round_id_seq OWNED BY round.id;

--
-- TOC entry 2033 (class 2604 OID 43398)
-- Name: game id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY game ALTER COLUMN id SET DEFAULT nextval('game_id_seq'::regclass);


--
-- TOC entry 2034 (class 2604 OID 43411)
-- Name: picture id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY picture ALTER COLUMN id SET DEFAULT nextval('picture_id_seq'::regclass);


--
-- TOC entry 2035 (class 2604 OID 43422)
-- Name: question id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question ALTER COLUMN id SET DEFAULT nextval('question_id_seq'::regclass);


--
-- TOC entry 2036 (class 2604 OID 43433)
-- Name: round id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY round ALTER COLUMN id SET DEFAULT nextval('round_id_seq'::regclass);


--
-- TOC entry 2039 (class 2606 OID 43400)
-- Name: game game_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY game
    ADD CONSTRAINT game_pkey PRIMARY KEY (id);


--
-- TOC entry 2041 (class 2606 OID 43405)
-- Name: handled_update handled_update_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY handled_update
    ADD CONSTRAINT handled_update_pkey PRIMARY KEY (update_id);


--
-- TOC entry 2043 (class 2606 OID 43416)
-- Name: picture picture_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY picture
    ADD CONSTRAINT picture_pkey PRIMARY KEY (id);




--
-- TOC entry 2045 (class 2606 OID 43427)
-- Name: question question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question
    ADD CONSTRAINT question_pkey PRIMARY KEY (id);


--
-- TOC entry 2047 (class 2606 OID 43435)
-- Name: round round_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY round
    ADD CONSTRAINT round_pkey PRIMARY KEY (id);


--
-- TOC entry 2051 (class 2606 OID 43441)
-- Name: question fk4v10lbmcif0p426f50hu6a1ue; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY question
    ADD CONSTRAINT fk4v10lbmcif0p426f50hu6a1ue FOREIGN KEY (picture_id) REFERENCES picture(id);


--
-- TOC entry 2050 (class 2606 OID 43436)
-- Name: game fkeun7n15w13jvkfd1vtxxm1x6u; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY game
    ADD CONSTRAINT fkeun7n15w13jvkfd1vtxxm1x6u FOREIGN KEY (picture_id) REFERENCES picture(id);


--
-- TOC entry 2053 (class 2606 OID 43451)
-- Name: round fkmlbyw7m6kgtm472xr5r4bnh1m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY round
    ADD CONSTRAINT fkmlbyw7m6kgtm472xr5r4bnh1m FOREIGN KEY (question_id) REFERENCES question(id);


--
-- TOC entry 2052 (class 2606 OID 43446)
-- Name: round fkppxonwn9e98lccy46m2eve67m; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY round
    ADD CONSTRAINT fkppxonwn9e98lccy46m2eve67m FOREIGN KEY (game_id) REFERENCES game(id);


-- Completed on 2017-10-21 22:28:11

--
-- PostgreSQL database dump complete
--

