--liquibase formatted sql

--changeset Sobolev:2

insert into picture values (10121,'К. К. Гампельн. Сцена из Отечественной войны 1812 года. 1830 г.',CURRENT_TIMESTAMP,'Gampeln_K.-Scena_iz_Otechestvennoj_vojny_1812_goda.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько человек изображено на картине?',10121);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Желтый','Зеленый','Какого цвета мундир у всадника?',10121);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Его там нет','В какой части картины расположен лес?',10121);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На всадника','На пушку','Ее глаза закрыты','Куда обращен взгляд девушки на картине?',10121);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Цилиндрической','Треугольной','Квадратной','Какой формы головной убор всадника?',10121);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Золотистая','Серебристая','Красная','Какого цвета накидка на плечах у девушки?',10121);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пушка','Ограда','Костер','Через что перепрыгивает конь на картине?',10121);

insert into picture values (10122,'Н. Д. Дмитриев-Оренбургский. Генерал Н.Д.Скобелев на коне. 1883 г.',CURRENT_TIMESTAMP,'Dmitriev-Orenburgskij_N.-General_N._D._Skobelev_na_kone.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько лошадей на картине?',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белый','Черный','Коричневый','Какого цвета конь у генерала?',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Крест','Корона','Двуглавый орел','Что изображено на знамени?',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Синяя','Черная','Какого цвета фуражка у генерала?',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сабля','Пистолет','Бинокль','Что в правой руке у генерала?',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Усы','Борода','Усы и борода','Отличительная особенность во внешности генерала?',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По траве','По воде','По снегу','Конь генерала скачет...',10122);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Оренбургский','Московский','Новгородский','Фамилия автора картины Дмитриев-...',10122);

insert into picture values (10123,'К. А. Зеленцов. Мастерская художника Петра Васильевича Басина. 1833 г.',CURRENT_TIMESTAMP,'Zelencov_K.-Masterskaya_hudozhnika_Petra_Vasilevicha_Basina.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('5','6','7','Сколько человек на картине?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','Ни одной','Сколько статуй на картине?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Цилиндр','Картуз','Шапка-ушанка','Какой головной убор изображен на полу за натурщиком?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Он босой','Лапти','Валенки','Во что обут натурщик?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Синий','Зеленый','Какого цвета диван на заднем плане?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вверху справа','Вверху слева','Вверху по центру','Где на картине расположено окно?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В кресле','На табурете','На диване','Где сидит мужчина в белых брюках?',10123);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Портрет','Пейзаж','Натюрморт','Что на картине, лежащей в открытой папке на полу?',10123);

insert into picture values (10124,'А. И. Иванов. Переправа Н.В.Гоголя через Днепр. 1845 г.',CURRENT_TIMESTAMP,'Ivanov_A.-Pereprava_N._V._Gogolya_cherez_Dnepr.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('6','4','8','Сколько человек изображено на картине?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Низко','Высоко','За ним','Где по отношению к горизонту находится солнце?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','По центру','В какой части картины изображены летящие птицы?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько весел у лодки, изображенной на переднем плане?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Его там нет','Слева','Справа','Где на картине изображен мост?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Шляпа','Берет','Голова не покрыта','Что на голове у мужчины стоящего в хвосте лодки, изображенной ближе к зрителю?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Красная','Черная','Какого цвета рубаха у мужчины, сидящего на носу лодки с Гоголем?',10124);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Через Днепр','Через Ворсклу','Через Дунай','Картина называется "Переправа Н.В.Гоголя..."',10124);

insert into picture values (10125,'А. И. Иванов. Крещение великого князя Владимира в Корсуни. 1829 г.',CURRENT_TIMESTAMP,'Ivanov_A.-Kreshchenie_velikogo_knyazya_Vladimira_v_Korsuni.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На ступенях','В воде','На песке','Где стоит великий князь?',10125);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На плече князя','На голове князя','На запястье князя','Где находится левая рука митрополита?',10125);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Желтая','Зеленая','Какого цвета накидка у великого князя?',10125);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Зеленая','Синяя','Какого цвета ряса у метрополита?',10125);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько зажженных свечей на картине?',10125);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Держит младенца','Крестится','Приветствует князя','Что делает левой рукой сидящая женщина?',10125);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На левом бедре','В левой руке','В правой руке','Где расположен мечь князя?',10125);

insert into picture values (10126,'А. Д. Кившенко. Жнитво. 1878 г.',CURRENT_TIMESTAMP,'Kivshenko_A.-ZHnitvo.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('6','7','8','Сколько человек возвращается с жатвы?',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По тропинке','По мосту','Вброд','Люди возвращаются...',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько человек идет с покрытой головой?',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Красный','Желтый','Какого цвета платок у девочки на переднем плане картины?',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кувшин','Корзина','Букет цветов','Что у девочки в платке в левой руке?',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ясная','Пасмурная','Дождливая','Какая погода на картине?',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Синий','Зеленый','Какого цвета передник у девочки в платке?',10126);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Ее там нет','С какой стороны на картине расположена река?',10126);

insert into picture values (10127,'А. Д. Кившенко. Военный совет в Филях. 1880 г.',CURRENT_TIMESTAMP,'Kivshenko_A.-Voennyj_sovet_v_Filyah.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('12','10','14','Сколько человек изображено на картине?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Зеленая','Красная','Белая','Какого цвета занавеска на окне?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голубая','Красная','Желтая','Какого цвета лента на мундире у военного, сидящего спиной к зрителю?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','3','4','Сколько людей на картине стоит?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пишет','Курит','Пьет','Что делает военный, стоящий слева, у печи?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На подлокотнике','У виска','На лацкане мундира','Где расположена правая рука Кутузова?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Посередине','Где на картине расположен иконостас?',10127);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1880','1820','1812','В каком году написана картина?',10127);

insert into picture values (10128,'А. И. Корзухин. Воскресный день. 1884 г.',CURRENT_TIMESTAMP,'Korzuhin_A.-Voskresnyj_den.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('8','6','10','Сколько людей изображено на картине?',10128);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','Ее там нет','В какой части картины находится река?',10128);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Скрипка','Гармонь','Гитара','Какого музыкального инструмента нет на картине?',10128);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черное','Красное','Зеленое','Какого цвета платье у девушки, которая танцует?',10128);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Платок','Шляпа','Цветок','Что в левой руке у танцующей девушки?',10128);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По центру','Слева','Справа','Где на картине расположен самовар?',10128);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Чепец','Шляпа','Венок','Что на голове у женщины с ребенком?',10128);

insert into picture values (10129,'И. Н. Крамской. Портрет художника Ивана Ивановича Шишкина. 1873 г.',CURRENT_TIMESTAMP,'Kramskoj_I.-Portret_hudozhnika_Ivana_Ivanovicha_SHishkina.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','За ним','С какой стороны на картине по отношению к герою падает тень?',10129);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Усы и борода','Усы','Борода','Какая отличительная особенность во внешности героя?',10129);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черная','Синяя','Зеленая','Какого цвета шляпа у мужчины?',10129);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сапоги','Сандалии','Ботинки','Во что обут мужчина?',10129);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В сторону','Под ноги','Вверх','Куда смотрит мужчина?',10129);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сигарета','Карандаш','Фляга','Что у мужчины в левой руке?',10129);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На левом плече','На правом плеча','На земле','Где находится портфель мужчины?',10129);

insert into picture values (10130,'Н. С. Крылов. Зимний пейзаж. 1827 г.',CURRENT_TIMESTAMP,'Krylov_N.-Zimnij_pejzazh.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько деревьев расположено слева на переднем плане?',10130);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На картине нет птиц','Воробьи','Вороны','Какие птицы изображены на картине?',10130);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черный','Белый','Коричневый','Какой окрас у лошади на переднем плане?',10130);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Коромысло','Мешок','Метла','Что на плече у женщины, направляющейся к мужчине с конем?',10130);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Синий','Белый','Какого цвета платок у женщины с санями?',10130);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Влево','Вправо','Теней не видно','Куда на картине падают тени?',10130);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','3','5','Сколько людей изображено на переднем плане на картине?',10130);



insert into picture values (10131,'И. А. Акимов. Самосожжение Геркулеса на костре в присутствии его друга Филоктета. 1782 г.',CURRENT_TIMESTAMP,'Akimov_I.-Samosozhzhenie_Gerkulesa_na_kostre_v_prisutstvii_ego_druga_Filokteta.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лук','Арбалет','Копье','Какое оружие изображено в левом нижнем углу картины?',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Факел','Меч','Кубок','Что Филоктет держит в правой руке?',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вторая голова','Хвост','Крылья','Что отсутствует у чудовища на шлеме Филоктета?',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Желтая','Зеленая','Какого цвета накидка Филоктета?',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Указывает на факел','Сжата в кулак','Поднята к небу','Правая рука Геракулеса...',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На Филоктета','На небо','Под ноги','Куда смотрит Геркулес?',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бревна','Камни','Сено','Что под ногами у Геракулеса?',10131);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1782','1872','1827','В каком году написана картина?',10131);

insert into picture values (10132,'А. И. Корзухин. Возвращение из города. 1870 г.',CURRENT_TIMESTAMP,'Korzuhin_A.-Vozvrashchenie_iz_goroda.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('5','4','6','Сколько человек изображено на картине?',10132);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голубая','Зеленая','Желтая','Какого цвета прикраватная занавеска?',10132);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Наливает воду','Разжигает печь','Подметает','Что делает пожилая женщина на картине?',10132);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сапоги','Лапти','Он босой','Во что обут мужчины?',10132);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По центру','Слева','Справа','Где на картине изображена детская колыбель?',10132);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На полу','На руках у сестры','В колыбели','Где на картине изображен младенец?',10132);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бублики','Газету','Шапку','Что держит мужчина в левой руке?',10132);

insert into picture values (10133,'Д. Г. Левицкий. Портрет Екатерины II. 1787 г.',CURRENT_TIMESTAMP,'Levickij_D.-Portret_Ekateriny_II.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Меч','Веер','Яблоко','Что держит императрица в правой руке?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Радуга','Гроза','Метеоритный дождь','Какое природное явление изображено на заднем плане?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лавровый венец','Корона','Шляпа','Что на голове у императрицы?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черно-оранжевая','Голубая','Бело-сине-красная','Какого цвета лента на груди императрицы?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На зрителя','На правую руку','К небу','Куда обращен взгляд императрицы?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ничего','Знамена','Оружие','Что лежит у ног императрицы?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтая','Зеленая','Бордовая','Какого цвета бахрома на наряде императрицы?',10133);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Екатринины II','Екатринины I','Елизаветы I','Картина называется "Портрет..."',10133);

insert into picture values (10134,'Н. Д. Лосев. Блудный сын. 1882 г.',CURRENT_TIMESTAMP,'Losev_N.-Bludnyj_syn.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('5','4','6','Сколько людей на картине?',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пес','Кот','Осел','Какое животное изображено на картине?',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сложены в молитве','Обхватывают голову','Подняты к небу','Руки у пожилой женщины...',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белое','Серое','Голубое','Какого цвета одеяние у старца?',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голубая','Зеленая','Красная','Какого цвета блуза у молодой женщины в платке?',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Браслет','Серьги','Ожерелье','Какого украшения нет на молодой женщине в платке?',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Дуб','Пальма','Сосна','Какое дерево изображено на картине?',10134);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сандалии','Шляпа','Трость','Какого предмета нет на картине?',10134);

insert into picture values (10135,'А. П. Лосенко. Авраам приносит в жертву сына своего Исаака. 1765 г.',CURRENT_TIMESTAMP,'Losenko_A.-Avraam_prinosit_v_zhertvu_syna_svoego_Isaaka.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кинжал','Меч','Факел','Что держит Авраам в правой руке?',10135);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Баран','Волк','Змея','Какое животное изображено на картине?',10135);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтого','Зеленого','Синего','Какого цвета головной убор Авраама?',10135);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сложены за спиной','Сложены на груди','Обхватывают голову','Где расположены руки Исаака?',10135);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Черная','Красная','Какого цвета повязка на глазах у Исаака?',10135);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лежит на бревнах','Привязан к столбу','Сидит под деревом','Исаак...?',10135);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На ангела','На сына','На зрителя','Куда обращен взгляд Авраама?',10135);

insert into picture values (10136,'А. П. Лосенко. Прощание Гектора с Андромахой. 1773 г.',CURRENT_TIMESTAMP,'Losenko_A.-Proshchanie_Gektora_s_Andromahoj.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько лошадей изображено на картине?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лев','Змея','Орел','Какое животное изображено на щите?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','Ни одной','Сколько статуй изображено на картине?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Желтая','Голубая','Какого цвета накидка на плечах у Гектора?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('К небу','На зрителя','На жену','Куда обращен взгляд Гектора?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтый','Зеленый','Красный','Какого цвета наряд у Андромахи?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Плачет','Смеется','Молится','Что делает женщина, за спиной у Андромахи?',10136);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На левом бедре','На правом бедре','В руке','Где меч Гектора?',10136);

insert into picture values (10137,'И. В. Лучанинов. Благословение ополченца 1812 года. 1812 г.',CURRENT_TIMESTAMP,'Luchaninov_I.-Blagoslovenie_opolchenca_1812_goda.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('6','5','7','Сколько человек на картине?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Браслет','Серьги','Ожерелье','Какого украшения нет на молодой женщине, изображенной слева?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красный','Синий','Зеленый','Какого цвета сарафан на молодой женщине, изображенной слева?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Монеты','Спички','Яблоки','Что рассыпано на столе?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Красная','Там нет скатерти','Какого цвета скатерть на столе?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кувшин','Икона','Свеча','Что в руках у женщины, изображенной на картине справа?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько человек на картине с непокрытой головой?',10137);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Он босой','Лапти','Сапоги','Во что обут мужчина с иконой?',10137);

insert into picture values (10138,'В. Г. Перов. Охотники на привале. 1871 г.',CURRENT_TIMESTAMP,'Perov_V.-Ohotniki_na_privale.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На левой','На правой','У него нет кольца','На какой руке кольцо у мужчины слева?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Папироса','Курительная трубка','Фляга','Что держит мужчина справа в правой руке?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Фуражка','Шляпа','Голова не покрыта','Что на голове у мужчины справа?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько ружий дежит на переднем плане?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Его там нет','Слева','Справа','Где на картине расположен костер?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пень','Рюкзак','Ящик','На что облокотился мужчина по центру?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Улыбается','Зевает','Спит','Что делает мужчина по центру?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пасмурный','Солнечный','Туманный','Какой день изображен на картине?',10138);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Перов','Репин','Серов','Какая фамилия у автора картины?',10138);

insert into picture values (10139,'В. Г. Перов. Приезд гувернантки в купеческий дом. 1866 г.',CURRENT_TIMESTAMP,'Perov_V.-Priezd_guvernantki_v_kupecheskij_dom.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('9','11','6','Сколько человек изображено на картине?',10139);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько свечей в подсвечнике на стене?',10139);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Коричневая','Желтая','Какого цвета дверь слева на картине?',10139);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Портрет','Пейзаж','Натюрморт','Что изображено на картине, висящей на стене?',10139);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Розовая','Голубая','Зеленая','Какого цвета юбка на девушке, изображенной крайней справа?',10139);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бабочка','Пояс','Очки','Какой аксессуар есть на молодом человеке в черном пальто, прячущем руки за спину?',10139);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Плед','Газета','Ножницы','Что лежит на ближнем к зрителю стуле?',10139);

insert into picture values (10140,'В. Г. Перов. Птицелов. 1870 г.',CURRENT_TIMESTAMP,'Perov_V.-Pticelov.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько клеток для птиц на картине?',10140);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Влево','Вверх','Друг на друга','Куда на картине смотрят главные герои?',10140);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На животе','На спине','На боку','В какой позе лежит пожилой мужчина?',10140);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтая','Синяя','Красная','Какого цвета подкладка пальто пожилого мужчины?',10140);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Шарф','Шляпа','Перчатки','Что из перечисленного есть в эпипировке мальчика?',10140);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Птичья клетка','Ствол дерева','Колено','На что опирается правая рука мальчика?',10140);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Его там нет','Слева','Справа','Где на картине изображен пес?',10140);



insert into picture values (10141,'И. М. Прянишников. В 1812 году. 1874 г.',CURRENT_TIMESTAMP,'Pryanishnikov_I.-V_1812_godu.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('7','6','8','Сколько человек изображено на переднем плане картины?',10141);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('По снегу','По льду','По воде','Процессия идет...',10141);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','В небе нет птиц','Где на картине изображена стая летящих птиц?',10141);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Вилы','Лопата','Топор','Что на плече у женщины, стоящей на переднем плане спиной к зрителю?',10141);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белые','Черные','Зеленые','Какого цвета штаны у мужчины в красном мундире?',10141);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Платок','Кивер','Шапка-ушанка','Что на голове у солдата с барабаном?',10141);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('5','3','6','Сколько пленных на переднем плане?',10141);

insert into picture values (10142,'Г. И. Семирадский. Опасный урок. 1880 г.',CURRENT_TIMESTAMP,'Semiradskij_G.-Opasnyj_urok.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько человек изображено на картине?',10142);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько стрел попали в мишень?',10142);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Конь','Бык','Дракон','Какое животное показано на скульптуре?',10142);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синяя','Желтая','Зеленая','Какого цвета накидка у мальчика?',10142);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красного','Белого','Синего','Какого цвета обувь на ногах у мужчины?',10142);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голова','Яблоко','Круг','Что изображено на мишени?',10142);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Его там нет','В какой части картины изображено море?',10142);

insert into picture values (10143,'Е. С. Сорокин. Ян Усмовец, удерживающий быка. 1849 г.',CURRENT_TIMESTAMP,'Sorokin_E.-YAn_Usmovec,_uderzhivayushchij_byka.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Он босой','Сандалии','Сапоги','Что на ногах у Яна Усмовца?',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Брюнет','Блондин','Рыжий','Ян Усмовец...',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Мужчина','Женщина','Младенец','Кто лежит на земле у ног главного героя?',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белая','Синяя','Черная','Какого цвета накидка, прикрывающая главного героя?',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На холме','На трибунах','На берегу реки','Где расположились люди, наблюдающие за сражением главного героя с быком?',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Папоротник','Оливковое дерево','Куст розы','Какое растение изображено на картине?',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Он без оружия','Меч','Кинжал','Какое оружие в руках у главного героя?',10143);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сорокин','Воробьев','Голубев','Какая фамилия у автора картины?',10143);

insert into picture values (10144,'А. С. Степанов. Утренний привет. 1897 г.',CURRENT_TIMESTAMP,'Stepanov_A.-Utrennij_privet.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько людей изображено на картине?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько собак изображено на картине?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Скворечник','Флюгер','Флаг','Что виднеется над крышей сарая?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В кармане','У виска','За спиной','Где находится левая рука пожилого мужчины?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синие','Красные','Белые','Какого цвета брюки на пожилом мужчине?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Карета','Велосипед','Кресло-каталка','Что стоит под навесом сарая?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('У корыта','У забора','Под деревом','Где на картине изображены гуси?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Утки','Голуби','Индюки','Какие птицы отстутсвуют на картине?',10144);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Утро','День','Вечер','Какое время суток изображено на картине?',10144);

insert into picture values (10145,'В. И. Суриков. Боярыня Морозова. 1887 г.',CURRENT_TIMESTAMP,'Surikov_V.-Boyarynya_Morozova.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сани','Карета','Повозка','Средство передвижения боярыни?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','Ни одной','Сколько детей бежит за боярыней?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черный','Белый','Красный','Какого цвета наряд у боярыни?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сложенные пальцы','Открытую ладонь','Кулак','Что демонстрирует поднятая правая рука боярыни?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Алебарда','Копье','Ружье','Какое оружие на плече стражника?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Крестит боярыню','Показывает кулак','Бросает камень','Что делает нищий в правом углу картины?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтый','Черный','Красный','Какого цвета платок на девушке в синем пальто, склонившей голову?',10145);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1887','1788','1878','В каком году написана картина?',10145);

insert into picture values (10146,'И. Ф. Тупылев. Александр Македонский перед Диогеном. 1787 г.',CURRENT_TIMESTAMP,'Tupylev_I.-Aleksandr_Makedonskij_pered_Diogenom.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('8','6','10','Сколько человек изображено на картине?',10146);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','3','4','Сколько лошадей изображено на картине?',10146);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бочка','Дерево','Булыжник','Что находится за спиной Диогена?',10146);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ясное с облаками','Ясное без облаков','Пасмурное','Небо на картине...',10146);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На рукописи','На груди','На голове','Где лежит левая рука Диогена?',10146);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Копье','Меч','Секира','Какое оружие в руке воина на коне?',10146);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Черная','Голубая','Какого цвета накидка у Александра Македонского?',10146);

insert into picture values (10147,'П. А. Федотов. Свежий кавалер. 1848 г.',CURRENT_TIMESTAMP,'Fedotov_P.-Svezhij_kavaler.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Гитара','Скрипка','Балалайка','Какой музыкальный инструмент изображен на картине?',10147);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Кот','Собака','Крыса','Какое животное изображено на картине?',10147);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько человек изображено на картине?',10147);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Колбаса','Сыр','Хлеб','Что порезано на газете?',10147);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В полоску','В клетку','В горошек','Какой расцветки халат главного героя?',10147);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сапог','Графин','Полотенце','Что держит девушка в левой руке?',10147);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Он босой','Туфли','Комнатные тапочки','Во что обут главный герой?',10147);

insert into picture values (10148,'П. А. Федотов. Завтрак аристократа. 1848 г.',CURRENT_TIMESTAMP,'Fedotov_P.-Zavtrak_aristokrata.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Пудель','Такса','Бульдог','Пес какой породы изображен на картине?',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Зеленое','Синее','Красное','Какого цвета кресло на картине?',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ваза','Самовар','Бутылка','Что стоит под столом?',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Устрицы','Креветки','Крабы','Что написано на листовке, лежащей на стуле?',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красные','Синие','Желтые','Какого цвета штаны на главном герое?',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На правой','На левой','На обеих','На какой руке у главного героя перстень?',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Жует','Пьет','Смеется','Главный герой...',10148);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Книгой','Полотенцем','Газетой','Чем главный герой пытается накрыть одно из блюд?',10148);

insert into picture values (10149,'И. И. Фирсов. Юный живописец. 1765 г.',CURRENT_TIMESTAMP,'Firsov_I.-YUnyj_zhivopisec.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько человек на картине?',10149);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','3','4','Сколько картин висит на стене?',10149);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Слева','Справа','Посередине','Где на картине изображено окно?',10149);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Браслет','Серьги','Ожерелье','Какого из украшений нет у женщины?',10149);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белый','Розовый','Красный','Какого цвета передник на женщине?',10149);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Коричневый','Черный','Какого цвета костюм художника?',10149);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ваза','Бюст','Чемодан','Какой из перечисленных предметов отсутствует на картине?',10149);

insert into picture values (10150,'К. К. Штейбен. Портрет А.В.Суворова. 1815 г.',CURRENT_TIMESTAMP,'Firsov_I.-Portret_Suvorova_1815.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Дуб','Береза','Сосна','Какое дерево изображено на картине?',10150);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В сторону','На карту','На зрителя','Куда смотрит Суворов?',10150);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На карте','На груди','На поясе','Где находится правая рука Суворова?',10150);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Синяя','Зеленая','Какого цвета подкладка мундира Суворова?',10150);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Справа','Слева','Там нет сражения','С какой стороны картины изображено сражение?',10150);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белые','Черные','Красные','Какого цвета штаны Суворова?',10150);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','5','6','Сколько орденов висит на самом мундире Суворова?',10150);