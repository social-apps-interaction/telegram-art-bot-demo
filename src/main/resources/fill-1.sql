﻿insert into picture values (10001,'Н. Ю. Анохин. В старом доме Ракитиных. 1998 г.',CURRENT_TIMESTAMP,'Anohin_N.-V_starom_dome_Rakitinyh.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белые и зеленые','Белые и синие','Белые и черные','Какого цвета клетки одеяла, на котором сидит кот?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько отверстий видно в печи?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красная','Белая','Красно-белая','Какого цвета кофта у девочки?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Круга','Ромба','Прямоугольника','Какой фигуры нет на двери?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ни одного','Одно','Два','Сколько окон видно целиком?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Стол','Стул','Кровать','Какого предмета нет на картине?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Ничего','Котелок','Книга','Что находится на ближней к зрителю полке?',10001);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сандалии','Ботинки','Она босая','Во что обута девочка?',10001);

insert into picture values (10002,'И. П. Богданов. За расчетом. 1890 г.',CURRENT_TIMESTAMP,'Bogdanov_I.-Za_raschetom.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Клетка','Занавеска','Полотенце','Что висит на окне?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Розовая','Белая','Серая','Какого цвета рубашка у хозяина?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько стульев в комнате?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Шапку','Бумагу','Ничего','Что держит крестьянин в правой руке?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Платок','Рубашка','Одеяло','Что висит на красной занавеске?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На коврике','На полу','На пороге','На чем стоит крестьянин?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Приоткрыта','Открыта полностью','Плотно закрыта','Дверь в комнату...?',10002);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На крестьянина','На хозяйку','В окно','Хозяин смотрит...',10002);

insert into picture values (10003,'Н. П. Богданов-Бельский. Виртуоз. 1891 г.',CURRENT_TIMESTAMP,'Bogdanov-Bel''skij_N.-Virtuoz.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','5','3','Сколько мальчиков изображено на картине?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На балалайке','На гитаре','На мандалине','На чем играет один из мальчиков?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','3','5','Сколько детей сидят?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Порвана','Испачкана','Надета наизнанку','Что с клетчатой рубашкой на одном из мальчиков?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синяя','Серая','Белая','Какого цвета юбка у девочки?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белые','Синие','Кориченвые','У одного из мальчиков штаны отличаются от остальных. Какого они цвета?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('У троих','У двоих','У четверых','У скольких детей нет головных уборов?',10003);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Березы','Тополя','Дубы','Какие деревья окружают детей?',10003);

insert into picture values (10004,'Н. П. Богданов-Бельский. Новая сказка. 1891 г.',CURRENT_TIMESTAMP,'Bogdanov-Bel''skij_N.-Novaya_skazka.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Теленка','Козу','Барана','Кого видно в дверном проеме?',10004);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На тетрадях','На коврике','На голом полу','На чем лежит кот?',10004);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','4','2','Сколько детей сидят?',10004);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Темные','Светлые','Рыжие','Какие волосы у читающего мальчика?',10004);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Новая сказка','Чтение','Одни дома','Как называется картина?',10004);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Лежит','Стоит','Перевернуто','Ведро на полу...',10004);

insert into picture values (10005,'Н. П. Богданов-Бельский. Ученицы. 1901 г.',CURRENT_TIMESTAMP,'Bogdanov-Bel''skij_N.-Uchenicy.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Крестик','Цепочка','Оберег','Что висит на шее у одной из девочек?',10005);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На шкаф','На стену','На печь','На что облокачиваются девочки?',10005);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Клетка','Полоска','Горошек','Какой рисунок на юбках девочек?',10005);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Рыжая','Белая','Красная','Какого цвета скамья, на которой сидят девочки?',10005);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Раскрыта','Закрыта','Приоткрыта','В каком состоянии находится книга у девочки в черном платке?',10005);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1901','1891','1896','В каком году написана картина?',10005);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Листок бумаги','Икона', 'Полотенце', 'Что висит над головой одной из девочек?',10005);

insert into picture values (10006,'С. И. Васильковский. Казачий двор. 1880-1890 гг.',CURRENT_TIMESTAMP,'Vasil''kovskij_S.-Kozachij_dvor.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','5','3','Сколько людей стоит у телеги?',10006);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','1','Сколько построек изображено на картине?',10006);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синее с облаками','Синее без облаков','Полностью затянуто облаками','Какое небо?',10006);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','3','2','Сколько окон у ближнего дома?',10006);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Бело-красная','Бело-зеленая','Бело-синяя','Каких цветов юбка на женщине?',10006);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('С. И. Васильковский','С. И. Васильев','С. И. Василенко','Кто автор картины?',10006);

insert into picture values (10007,'В. М. Васнецов. Аленушка. 1881 г.',CURRENT_TIMESTAMP,'Vasnecov_V.-Alenushka.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На камне','На земле','На бревне','На чем сидит Аленушка?',10007);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Аленушка босая','В лапти','В черевички','Во что обуты ноги Аленушки?',10007);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Еловый','Сосновый','Осиновый','Какой лес изображен на заднем плане?',10007);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Темная','Зеленая','Голубая','Какого цвета вода?',10007);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Опавшие листья','Кувшинки','Ветки','Что плавает по воде?',10007);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Свободно развеваются','Перетянуты лентой','Выбиваются из-под платка','Волосы Аленушки...?',10007);

insert into picture values (10008,'В. М. Васнецов. Богатыри. 1898 г.',CURRENT_TIMESTAMP,'Vasnecov_V.-Bogatyri.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черного','Белого','Рыжего','Какого цвета конь у Ильи Муромца?',10008);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Зеленые','Красные','Черные','Какого цвета сапоги у Добрыни Никитича?',10008);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Алеши Поповича','Ильи Муромца','Добрыни Никитича','Чей конь ниже всех опустил голову?',10008);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Илья Муромец','Добрыня Никитич','Алеша Попович','Какой из богатырей изображен в рукавицах?',10008);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Задняя правая','Задняя левая','Передняя правая','Какая из ног коня Ильи Муромца отличается по цвету от остальных?',10008);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Красное','Черное','Серое','Какого цвета древко копья Ильи Муромца?',10008);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Булава','Топор','Меч','Что висит на руке у Ильи Муромца?',10008);

insert into picture values (10009,'В. М. Васнецов. Витязь на распутье. 1882 г.',CURRENT_TIMESTAMP,'Vasnecov_V.-Vityaz''_na_rasput''e.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько воронов сидят на камнях?',10009);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько воронов изображено на картине?',10009);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Меча','Копья','Булавы','Какого оружия не видно у витязя?',10009);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Человека и лошади','Людей','Лошадей','Чьи черепа лежат у камня?',10009);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Копье','Меч','Булаву','Что витязь держит в правой руке?',10009);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Белого','Черного','Рыжего','Какого цвета грива и хвост коня?',10009);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Виктор Михайлович','Сергей Александрович','Петр Тимовеевич','Имя и отчество художника Васнецова?',10009);

insert into picture values (10010,'С. А. Виноградов. Барышня в сарафане. 1930 г.',CURRENT_TIMESTAMP,'Vinogradov_S.-Baryshnya_v_sarafane.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько яблок лежит на подоконнике?',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Скрещены, висят','Стоят ровно','Не видны','Ноги барышни...',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Завязан сзади','Завязан под подбородком','Не завязан','Платок у барышни...',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Желтая','Зеленая','Синяя','Какого цвета занавеска?',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Раскрыто наполовину','Закрыто','Раскрыто полностью','Окно в сад...',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На стуле','На скамье','На подоконнике','На чем сидит барышня?',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В двух кувшинах','В одном кувшине','В бутылке','В чем стоят цветы?',10010);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','4','Сколько бутылок стоит на столе?',10010);

insert into picture values (10011,'Е. Е. Волков. У монастыря. 1888 г.',CURRENT_TIMESTAMP,'Volkov_E.-U_monastyrya.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('К воде','К монастырю','К костру','Куда идет монах?',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Коромысло с ведрами','Одно ведро','Ведро и ковш','Что держит монах?',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('2','1','3','Сколько крестов видно на куполах монастыря?',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Серое','Голубое','Голубое с облаками','Небо на картине...',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Месяц','Солнце','Полную луну','Что видно на небе?',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Деревья','Монастырь','Монах','Что отражается в воде?',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Костер','Лес','Окно в доме','Что горит вдали?',10011);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Не имеет ворот','Закрыт воротами','Не виден','Вход в монастырь...',10011);

insert into picture values (10012,'М. М. Гермашев. Снег выпал. 1897 г.',CURRENT_TIMESTAMP,'Germashev_M.-Sneg_vypal.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько гусей что-то клюют в снегу?',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Прикуривает','Смотрит вдаль','Закрывает ворота','Что делает мужчина?',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Грабли','Вилы','Лестница','Какой предмет прислонен к стогу сена?',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('1','2','3','Сколько стогов сена стоит во дворе',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Несколько','Все','Ни один','Сколько гусей зашли во двор?',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Поле','Соседские дома','Озеро','Что находится сразу за забором?',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('4','2','6','Сколько бревен лежит во дворе?',10012);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Черного','Синего','Коричневого','Какого цвета шапка на мужчине?',10012);

insert into picture values (10013,'С. С. Егорнов. Цесаревич Алексей Николаевич.',CURRENT_TIMESTAMP,'Egornov_S.-Cesarevich_Aleksej_Nikolaevich.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Книга','Стопка бумаг','Очки','Что лежит на столе?',10013);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('3','2','1','Сколько видно стульев с голубой обивкой?',10013);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Неизвестно','Батальная сцена','Портрет царя','Что изображено на картине, висящей на заднем фоне?',10013);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Голубого','Красного','Белого','Какого цвета лента на груди цесаревича',10013);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Облокачиваясь рукой на стол','Руки по швам','Руки скрещены','В какой позе стоит цесаревич?',10013);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Фуражка','Книга','Ничего','Что лежит на ближнем к зрителю стуле?',10013);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синий','Черный','Зеленый','Цвет мундира цесаревича?',10013);

insert into picture values (10014,'К. А. Коровин. Портрет Федора Ивановича Шаляпина. 1911 г.',CURRENT_TIMESTAMP,'Korovin_K.-Portret_Fedora_Ivanovicha_SHalyapina.jpg');
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Веселое','Серьезное','Грустное','Выражение лица Шаляпина...',10014);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Синего','Черного','Малинового','Какого цвета галстук у Шаляпина?',10014);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('На стуле','На диване','На скамье','На чем сидит Шаляпин?',10014);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Правая лежит на левой','Левая лежит на правой','Стоят прямо','В каком положении находятся ноги Шаляпина?',10014);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Книги','Бокала красного вина','Цветов','Чего нет на столе?',10014);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('Сад','Улица','Река','Какой вид раскрывается из окна?',10014);
insert into question (answer,incorrect_answer_1,incorrect_answer_2,question,picture_id) values ('В кармане','На столе','Свисает','Левая рука Шаляпина находится...',10014);



