package telegram;

import com.pengrad.telegrambot.model.Message;

public interface MessageHandler {

    void handleStart(Message message);

    void handleGameStart(Message message);

    void handleImReadyButton(Message message);

    void handleAnswer(Message message);

    void handleShowPictureAfterGame(Message message);

    void handleStatisticAction(Message message);

    void handleEnd(Message message);

}
