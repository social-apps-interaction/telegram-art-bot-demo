package telegram.impl;

import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.response.SendResponse;
import com.vdurmont.emoji.EmojiParser;
import dao.GameDao;
import dao.PictureDao;
import dao.RoundDao;
import dao.entity.Game;
import dao.entity.Picture;
import dao.entity.Question;
import dao.entity.Round;
import dao.impl.GameDaoImpl;
import dao.impl.PictureDaoImpl;
import dao.impl.RoundDaoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import telegram.MessageHandler;
import telegram.MessageSenderService;
import util.Pauser;
import util.RatingCalculator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class MessageHandlerImpl implements MessageHandler {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private MessageSenderService messageSenderService = new ReplyKeyboardMessageSender();

    private PictureDao pictureDao = new PictureDaoImpl();
    private GameDao gameDao = new GameDaoImpl();
    private RoundDao roundDao = new RoundDaoImpl();
    private RatingCalculator ratingCalculator = new RatingCalculator(gameDao);

    private final String CORRECT_EMOJI = EmojiParser.parseToUnicode(":heavy_check_mark:");
    private final String INCORRECT_EMOJI = EmojiParser.parseToUnicode(":x:");
    private final String FINISH_EMOJI = EmojiParser.parseToUnicode(":checkered_flag:");
    private final String ART_EMOJI = EmojiParser.parseToUnicode(":art:");
    private final String QUESTION_EMOJI = EmojiParser.parseToUnicode(":question:");
    private final String TROPHY_EMOJI = EmojiParser.parseToUnicode(":trophy:");
    private final String CHECK_EMOJI = EmojiParser.parseToUnicode(":heavy_check_mark:");


    private final String NEWBIE_MESSAGE = "Привет! Правила игры таковы - я показываю тебе картину и ты изучаешь её. Как будешь готов я начну задавать вопросы о ней, естественно спрятав её от тебя ;)";
    private final String NO_MORE_QUESTIONS_FOR_PICTURE_MESSAGE = "Вы ответили верно на %d из %d вопросов";
    private final String GO_TO_MAIN_MENU_TEXT = "Возвращаемся в главное меню";

    @Override
    public void handleStart(Message message) {
        Long chatId = message.chat().id();
        Integer userId = message.from().id();

        if (gameDao.getUserGames(userId).isEmpty())
            messageSenderService.sendText(chatId, NEWBIE_MESSAGE);

        messageSenderService.sendMainMenu(chatId);
    }

    @Override
    public void handleGameStart(Message message) {
        Integer userId = message.from().id();
        Long chatId = message.chat().id();
        logger.info("Starting new game for user {}", userId);

        Optional<Game> currentGame = gameDao.getCurrentGame(userId);
        if (currentGame.isPresent()) {
            Game game = currentGame.get();
            logger.warn("User {} has active game {}. This game will be deactivated.", userId, game.getId());
            game.setIsFinished(true);
            gameDao.save(game);
        }

        Picture picture = pictureDao.getRandomNotPlayedPicture(userId);
        if (picture == null) {
            messageSenderService.sendText(chatId, "По какой-то причине не удаётся найти подходящую картину, возможно вы прошли их все.");
            messageSenderService.sendMainMenu(chatId);
            return;
        }

        SendResponse sendPictureResponse = messageSenderService.sendPictureWithButton(picture, chatId);
        Integer pictureMessageId = sendPictureResponse.message().messageId();

        Game game = new Game();
        game.setPicture(picture);
        game.setUserId(userId);
        game.setIsFinished(false);
        game.setPictureMessageId(pictureMessageId);
        gameDao.save(game);
    }

    @Override
    public void handleImReadyButton(Message message) {
        Long chatId = message.chat().id();
        Integer userId = message.from().id();
        Optional<Game> currentGameOptional = gameDao.getCurrentGame(userId);
        if (!currentGameOptional.isPresent()) {
            logger.error("User {} haven't active game");
            return;
        }
        Game game = currentGameOptional.get();
        messageSenderService.deleteMessage(chatId, game.getPictureMessageId());
        askNextQuestionIfExists(userId, chatId, game);
    }

    @Override
    public void handleAnswer(Message message) {
        Integer userId = message.from().id();
        logger.info("Start handling answer from user {}", userId);
        Long chatId = message.chat().id();
        String answer = message.text();

        Optional<Game> currentGameOptional = gameDao.getCurrentGame(userId);
        if (currentGameOptional.isPresent()) {
            Game game = currentGameOptional.get();
            List<Round> currentRound = game.getRounds().stream().filter(Round::getIsCurrent).collect(Collectors.toList());
            assert currentRound.size() == 1; // only 1 current round should exists
            Round round = currentRound.get(0);

            boolean isCorrect = false;
            String correctAnswer = round.getQuestion().getAnswer();
            if (correctAnswer.equalsIgnoreCase(answer)) {
                isCorrect = true;
                messageSenderService.sendText(chatId, CORRECT_EMOJI + " Верно!");
            } else {
                messageSenderService.sendText(chatId, String.format(INCORRECT_EMOJI + " Это неверный ответ, правильный ответ - %s", correctAnswer));
            }
            round.setUserAnswer(answer);
            round.setIsWon(isCorrect);
            round.setIsCurrent(false);
            roundDao.saveRound(round);

            askNextQuestionIfExists(userId, chatId, round.getGame());
        } else {
            logger.warn("User {} haven't active game, seems this message is not answer: {}", userId, answer);
        }
    }

    @Override
    public void handleShowPictureAfterGame(Message message) {
        Long chatId = message.chat().id();

        Integer userId = message.from().id();
        Optional<Game> userGameOptional = gameDao.getLastUserGame(userId);
        assert userGameOptional.isPresent();
        Game game = userGameOptional.get();
        messageSenderService.sendPictureAfterGame(game.getPicture(), chatId);
    }

    @Override
    public void handleEnd(Message message) {
        Long chatId = message.chat().id();
        Integer userId = message.from().id();

        Optional<Game> currentGameOptional = gameDao.getCurrentGame(userId);
        if (currentGameOptional.isPresent()) {
            Game game = currentGameOptional.get();
            game.setIsFinished(true);
            gameDao.save(game);
        }
        messageSenderService.sendText(chatId, GO_TO_MAIN_MENU_TEXT);
        messageSenderService.sendMainMenu(chatId);
    }

    @Override
    public void handleStatisticAction(Message message) {
        Long chatId = message.chat().id();
        Integer userId = message.from().id();
        logger.info("Handling 'Statistic' callback from user {}", userId);

        List<Game> finishedUserGames = gameDao.getUserGames(userId).stream().filter(Game::getIsFinished).collect(Collectors.toList());

        List<List<Round>> gamesLists = finishedUserGames.stream().map(Game::getRounds).collect(Collectors.toList());
        List<Round> allRounds = new ArrayList<>(); // todo rework with stream (don't know how)
        for (List<Round> rounds : gamesLists) {
            allRounds.addAll(rounds.stream().filter(r -> r.getIsWon() != null).collect(Collectors.toList()));
        }

        long gamesCount = allRounds.size();
        long correctAnswers = allRounds.stream().filter(r -> r.getIsWon()).count();
        Float successRate = (float) correctAnswers / (float) allRounds.size() * 100;
        Integer position = ratingCalculator.getUserCorrectAnswersRatingPosition(userId);

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format(TROPHY_EMOJI + " Вы занимаете %s место в общем рейтинге\n", position != null ? position : "последнее"));
        stringBuilder.append(String.format(CHECK_EMOJI + " Правильных ответов - %s%%\n", successRate.intValue()));
        stringBuilder.append(String.format(ART_EMOJI + " Изученных картин - %d\n", finishedUserGames.size()));
        stringBuilder.append(String.format(QUESTION_EMOJI + " Пройденных вопросов - %d\n", gamesCount));

        messageSenderService.sendStatisticPage(chatId, stringBuilder.toString());
    }

    private void askNextQuestionIfExists(Integer userId, Long chatId, Game game) {
        Picture picture = game.getPicture();
        logger.info("Looking for another question about picture={} for user {}", picture.getId(), userId);
        List<Question> notPlayedQuestions = getNotPlayedQuestions(game);

        if (!notPlayedQuestions.isEmpty()) {
            int answeredQuestionsCount = game.getRounds().size();
            int totalQuestionsCount = game.getPicture().getQuestions().size();
            String currentProgressMessage = String.format(" (%d/%d)", answeredQuestionsCount + 1, totalQuestionsCount);

            Question notPlayedQuestion = notPlayedQuestions.get(new Random().nextInt(notPlayedQuestions.size()));
            messageSenderService.sendQuestion(chatId, currentProgressMessage, notPlayedQuestion);

            Round round = new Round();
            round.setQuestion(notPlayedQuestion);
            round.setGame(game);
            round.setIsCurrent(true);
            roundDao.saveRound(round);
            logger.info("New question asked about picture={} for user {}", picture.getId(), userId);
        } else {
            game.setIsFinished(true);
            gameDao.save(game);

            List<Round> playedRounds = game.getRounds();
            long wonGames = playedRounds.stream().filter(g -> g.getIsWon()).count();
            Pauser.pause(Pauser.FINISH_GAME_PAUSE);
            messageSenderService.sendText(chatId, String.format(FINISH_EMOJI + " " + NO_MORE_QUESTIONS_FOR_PICTURE_MESSAGE, wonGames, playedRounds.size()));

            messageSenderService.sendFinishedGameMenu(chatId);
            logger.info("No more questions about picture={} for user {}. Game finished", picture.getId(), userId);
        }
    }

    private List<Question> getNotPlayedQuestions(Game game) {
        List<Question> pictureQuestions = game.getPicture().getQuestions();
        List<Round> playedRounds = game.getRounds();
        List<Question> notPlayedQuestions = new ArrayList<>();
        List<Question> playedQuestions = playedRounds.stream().map(Round::getQuestion).collect(Collectors.toList());

        for (Question question : pictureQuestions) {
            if (!playedQuestions.stream().map(Question::getId).collect(Collectors.toList()).contains(question.getId()))
                notPlayedQuestions.add(question);
        }

        return notPlayedQuestions;
    }


}
