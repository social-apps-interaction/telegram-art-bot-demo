package telegram.impl;

import com.pengrad.telegrambot.model.Message;
import dao.UserDao;
import dao.entity.User;
import dao.impl.UserDaoImpl;
import telegram.MessageHandler;

public class MessageHandlerUserSaverDecorator implements MessageHandler {

    private MessageHandler messageHandler;
    private UserDao userDao;

    public MessageHandlerUserSaverDecorator(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
        userDao = new UserDaoImpl();
    }

    private void saveOrUpdateUserInfo(Message message) {
        User user = new User(message.from());
        userDao.saveOrUpdate(user);
    }

    @Override
    public void handleStart(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleStart(message);
    }

    @Override
    public void handleGameStart(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleGameStart(message);
    }

    @Override
    public void handleImReadyButton(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleImReadyButton(message);
    }

    @Override
    public void handleAnswer(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleAnswer(message);
    }

    @Override
    public void handleShowPictureAfterGame(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleShowPictureAfterGame(message);
    }

    @Override
    public void handleEnd(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleEnd(message);
    }

    @Override
    public void handleStatisticAction(Message message) {
        saveOrUpdateUserInfo(message);
        messageHandler.handleStatisticAction(message);
    }

}
