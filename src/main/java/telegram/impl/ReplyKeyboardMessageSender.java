package telegram.impl;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.model.request.KeyboardButton;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.request.DeleteMessage;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.request.SendPhoto;
import com.pengrad.telegrambot.response.SendResponse;
import configuration.MainConfig;
import dao.entity.Picture;
import dao.entity.Question;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import telegram.Command;
import telegram.MessageSenderService;
import util.Pauser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ReplyKeyboardMessageSender implements MessageSenderService {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private static MainConfig mainConfig = ConfigFactory.create(MainConfig.class);
    private static TelegramBot telegramBot = TelegramBotAdapter.build(mainConfig.botToken());

    private static final String INVITATION_MESSAGE_MD = "Что делаем дальше?";



    @Override
    public void deleteMessage(Long chatId, Integer messageId) {
        logger.info("Delete message={} in chat={}", messageId, chatId);
        telegramBot.execute(new DeleteMessage(chatId, messageId));
    }

    @Override
    public SendResponse sendText(Long chatId, String text) {
        return telegramBot.execute(new SendMessage(chatId, text));
    }

    @Override
    public SendResponse sendMainMenu(Long chatId) {
        ArrayList<String> mainMenuActions = new ArrayList<>();
        mainMenuActions.add(Command.GAME_START.toString());
        mainMenuActions.add(Command.STATISTIC.toString());

        SendMessage sendMessage = new SendMessage(chatId, INVITATION_MESSAGE_MD).replyMarkup(createHorizontalMenuKeyboard(mainMenuActions));

        Pauser.pause(Pauser.MAIN_MENU_PAUSE);
        return telegramBot.execute(sendMessage);
    }

    @Override
    public SendResponse sendPictureWithButton(Picture picture, Long chatId) {
        String pictureUrl = mainConfig.imageHostingUrl().concat(picture.getFileName());
        logger.info("Create photo message with url ".concat(pictureUrl));

        ReplyKeyboardMarkup keyboard = createHorizontalMenuKeyboard(Arrays.asList(Command.READY.toString()));
        Pauser.pause();
        return checkResponse(telegramBot.execute(new SendPhoto(chatId, pictureUrl).caption(picture.getCaption()).replyMarkup(keyboard)));
    }

    @Override
    public SendResponse sendPictureAfterGame(Picture picture, Long chatId) {
        String pictureUrl = mainConfig.imageHostingUrl().concat(picture.getFileName());
        logger.info("Create photo message with url ".concat(pictureUrl));

        List<String> commands = new ArrayList<>();
        commands.add(Command.GAME_START.toString());
        commands.add(Command.STATISTIC.toString());
        ReplyKeyboardMarkup keyboard = createHorizontalMenuKeyboard(commands);

        Pauser.pause();
        return telegramBot.execute(new SendPhoto(chatId, pictureUrl).caption(picture.getCaption()).replyMarkup(keyboard));
    }

    @Override
    public SendResponse sendQuestion(Long chatId, Question question) {
        return sendQuestion(chatId, null, question);
    }

    @Override
    public SendResponse sendQuestion(Long chatId, String additionalMessage, Question question) {
        assert question.getAnswer() != null;
        assert question.getIncorrectAnswerA() != null;
        assert question.getIncorrectAnswerB() != null;

        ArrayList<String> answers = new ArrayList();
        answers.add(question.getAnswer());
        answers.add(question.getIncorrectAnswerA());
        answers.add(question.getIncorrectAnswerB());
        Collections.shuffle(answers);

        ReplyKeyboardMarkup keyboard = createVerticalMenuKeyboard(answers);

        Pauser.pause(Pauser.NEW_QUESTION_PAUSE);
        String messageText = question.getQuestion() + additionalMessage;
        return telegramBot.execute(new SendMessage(chatId, messageText).replyMarkup(keyboard));
    }

    @Override
    public SendResponse sendFinishedGameMenu(Long chatId) {
        List<String> menuCommands = new ArrayList<>();
        menuCommands.add(Command.SHOW_PICTURE.toString());
        menuCommands.add(Command.GAME_START.toString());
        menuCommands.add(Command.STATISTIC.toString());

        ReplyKeyboardMarkup keyboard = createVerticalMenuKeyboard(menuCommands);

        Pauser.pause();
        return telegramBot.execute(new SendMessage(chatId, INVITATION_MESSAGE_MD).replyMarkup(keyboard).parseMode(ParseMode.Markdown));
    }

    @Override
    public SendResponse sendStatisticPage(Long chatId, String userStatistic) {
        List<String> menuCommands = new ArrayList<>();
        menuCommands.add(Command.GAME_START.toString());

        Pauser.pause();
        return telegramBot.execute(new SendMessage(chatId, userStatistic).replyMarkup(createHorizontalMenuKeyboard(menuCommands)));
    }

    private ReplyKeyboardMarkup createHorizontalMenuKeyboard(List<String> commands) {
        KeyboardButton[][] keyboardButtons = new KeyboardButton[1][commands.size()];

        for (int i = 0; i < commands.size(); i++) {
            keyboardButtons[0][i] = new KeyboardButton(commands.get(i));
        }

        return new ReplyKeyboardMarkup(keyboardButtons).resizeKeyboard(true).oneTimeKeyboard(false);
    }

    private ReplyKeyboardMarkup createVerticalMenuKeyboard(List<String> commands) {
        KeyboardButton[][] keyboardButtons = new KeyboardButton[commands.size()][1];

        for (int i = 0; i < commands.size(); i++) {
            keyboardButtons[i][0] = new KeyboardButton(commands.get(i));
        }

        return new ReplyKeyboardMarkup(keyboardButtons).resizeKeyboard(true).oneTimeKeyboard(false);
    }

    private SendResponse checkResponse(SendResponse response) {
        if (!response.isOk()) {
            logger.error("Problem with message sending, telegram response: {}", response.description());
        }
        return response;
    }

}
