package telegram;

import com.pengrad.telegrambot.response.SendResponse;
import dao.entity.Picture;
import dao.entity.Question;

public interface MessageSenderService {

    void deleteMessage(Long chatId, Integer messageId);

    SendResponse sendPictureWithButton(Picture picture, Long chatId);

    SendResponse sendQuestion(Long chatId, Question question);

    SendResponse sendQuestion(Long chatId, String additionalMessage, Question question);

    SendResponse sendText(Long chatId, String text);

    SendResponse sendMainMenu(Long chatId);

    SendResponse sendStatisticPage(Long chatId, String userStatistic);

    SendResponse sendPictureAfterGame(Picture picture, Long chatId);

    SendResponse sendFinishedGameMenu(Long chatId);

}
