package telegram;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by sobolev on 9/7/2017.
 */
public enum Command {

    START("/start"), GAME_START("Новая игра"), END("/end"), STATISTIC("Статистика"), READY("Я готов"), SHOW_PICTURE("Покажи картину"), OTHER("other");

    private String commandValue;

    private static Map<String, Command> commandMap = new HashMap<>();

    static {
        for (Command command : Command.values()) {
            commandMap.put(command.toString(), command);
        }
    }

    Command(String command) {
        this.commandValue = command;
    }

    public static Command getEnum(String value) {
        Command command = commandMap.get(value);
        return command != null ? command : Command.OTHER;
    }

    @Override
    public String toString() {
        return commandValue;
    }

}
