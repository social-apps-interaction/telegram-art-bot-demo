package util;

public class Pauser {

    public static final int DEFAULT_PAUSE = 0;

    public static final int NEW_QUESTION_PAUSE = 333;

    public static final int MAIN_MENU_PAUSE = 333;

    public static final int FINISH_GAME_PAUSE = 111;

    public static void pause() {
        pause(DEFAULT_PAUSE);
    }

    public static void pause(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
