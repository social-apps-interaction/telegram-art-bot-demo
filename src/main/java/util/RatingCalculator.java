package util;

import dao.GameDao;
import dao.entity.Game;
import dao.entity.Round;
import dao.impl.GameDaoImpl;

import java.util.*;

public class RatingCalculator {

    private GameDao gameDao;

    public RatingCalculator() {
        gameDao = new GameDaoImpl();
    }

    public RatingCalculator(GameDao gameDao) {
        this.gameDao = gameDao;
    }

    /**
     * Return null if user didn't played at least 1 game
     *
     * @param targetUserId
     * @return
     */
    public Integer getUserCorrectAnswersRatingPosition(Integer targetUserId) {
        List<Integer> usersIds = gameDao.getUsersIds();
        if (!usersIds.contains(targetUserId)) return null;
        Map<Integer, Integer> usersWonRounds = new HashMap<>();
        for (Integer userId : usersIds) {
            int totalWonRounds = 0;
            List<Game> userGames = gameDao.getUserGames(userId);
            for (Game userGame : userGames) {
                int wonRoundsInGame = 0;
                List<Round> rounds = userGame.getRounds();
                if (rounds != null) {
                    for (Round round : rounds) {
                        if (round.getIsWon() != null && round.getIsWon())
                            wonRoundsInGame++;
                    }
                    totalWonRounds = totalWonRounds + wonRoundsInGame;
                }
            }
            usersWonRounds.put(userId, totalWonRounds);
        }

        Integer targetUserWonRounds = usersWonRounds.get(targetUserId);
        List<Integer> rating = new ArrayList<>();
        for (Integer userId : usersWonRounds.keySet()) {
            rating.add(usersWonRounds.get(userId));
        }
        Collections.sort(rating);
        Collections.reverse(rating);
        int targetUserRatingPosition = rating.indexOf(targetUserWonRounds);
        return targetUserRatingPosition + 1;
    }

}
