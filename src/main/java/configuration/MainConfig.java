package configuration;

import org.aeonbits.owner.Config;

@Config.Sources({
        "classpath:config.properties"
})
public interface MainConfig extends Config {

    @Key("bot-token")
    String botToken();

    @Key("telegram-endpoint")
    String telegramEndpoint();

    @Key("db-host")
    String dbHost();

    @Key("db-port")
    String dbPort();

    @Key("db-name")
    String dbName();

    @Key("db-username")
    String dbUsername();

    @Key("db-password")
    String dbPassword();

    @Key("image-hosting-url")
    String imageHostingUrl();
}
