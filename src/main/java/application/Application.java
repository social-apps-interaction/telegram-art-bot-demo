package application;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import configuration.MainConfig;
import dao.DatabaseManager;
import org.aeonbits.owner.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Application {

    private static Logger logger = LoggerFactory.getLogger(Application.class);

    private static MainConfig mainConfig = ConfigFactory.create(MainConfig.class);
    private static TelegramBot telegramBot = TelegramBotAdapter.build(mainConfig.botToken());

    public static void main(String[] args) {
        logger.info(String.format("Starting application with %s database", mainConfig.dbName()));

        try {
            logger.info("Migrate db schema if needed...");
            DatabaseManager.createOrUpdateDatabaseSchema();
            DatabaseManager.fillDbScript();
            logger.info("Migration successful");
        } catch (Exception e) {
            logger.error("Cant migrate DB with liquibase");
            throw new RuntimeException(e);
        }

        telegramBot.setUpdatesListener(new UpdatesHandler());

        logger.info("Application started. Telegram updates starts handling ...");
    }

}
