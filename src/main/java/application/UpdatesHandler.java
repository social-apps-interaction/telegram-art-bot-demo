package application;

import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import dao.UpdateDao;
import dao.entity.HandledUpdate;
import dao.impl.UpdateDaoImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import telegram.Command;
import telegram.MessageHandler;
import telegram.impl.MessageHandlerImpl;
import telegram.impl.MessageHandlerUserSaverDecorator;

import java.util.List;

public class UpdatesHandler implements UpdatesListener {

    private Logger logger = LoggerFactory.getLogger(getClass());
    private static UpdateDao updateDao = new UpdateDaoImpl();
    private MessageHandler messageHandler = new MessageHandlerUserSaverDecorator(new MessageHandlerImpl());

    @Override
    public int process(List<Update> updates) {
        Integer lastHandledUpdate = updateDao.getLastUpdateId();
        if (lastHandledUpdate == null) lastHandledUpdate = 0;
        for (Update update : updates) {
            Integer updateId = update.updateId();
            HandledUpdate updateEntity = new HandledUpdate();
            updateEntity.setUpdateId(updateId);
            updateEntity.setIsHandled(true);
            try {
                if (!updateDao.getUpdate(updateId).isPresent()) {

                    logger.info("HANDLING UPDATE ... {}", update);

                    Message mess = update.message();
                    if (mess != null) {
                        handleMessage(mess);
                    }
                }
            } catch (Exception e) {
                logger.error("Update is not handled {}\n{}", update, e.getMessage());
                updateEntity.setIsHandled(false);
                throw new RuntimeException(e);
            } finally {
                lastHandledUpdate = updateId;
                updateDao.save(updateEntity);
            }

        }
        return lastHandledUpdate;
    }

    private void handleMessage(Message message) {
        String text = message.text();
        Command command = Command.getEnum(text);


        switch (command) {
            case START:
                messageHandler.handleStart(message);
                break;
            case GAME_START:
                messageHandler.handleGameStart(message);
                break;
            case READY:
                messageHandler.handleImReadyButton(message);
                break;
            case SHOW_PICTURE:
                messageHandler.handleShowPictureAfterGame(message);
                break;
            case STATISTIC:
                messageHandler.handleStatisticAction(message);
                break;
            case END:
                messageHandler.handleEnd(message);
                break;
            case OTHER:
                messageHandler.handleAnswer(message);
                break;
            default:
                throw new IllegalArgumentException("Incorrect Command value");
        }


    }

}
