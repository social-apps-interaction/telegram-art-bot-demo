package dao;

import configuration.MainConfig;
import dao.entity.*;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.aeonbits.owner.ConfigFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.service.ServiceRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;

public class DatabaseManager {

    private static SessionFactory sessionFactory;
    private static MainConfig mainConfig = ConfigFactory.create(MainConfig.class);
    private static Logger logger = LoggerFactory.getLogger(DatabaseManager.class);

    public static Session openSession() {
        if (sessionFactory == null) sessionFactory = createSessionFactory();
        return sessionFactory.openSession();
    }

    public static void fillDbScript() throws Exception {
        if (sessionFactory == null) sessionFactory = createSessionFactory();
        ConnectionProvider connectionProvider = sessionFactory.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class);
        Connection connection = connectionProvider.getConnection();
        try {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));

            Liquibase picturesFilling = new Liquibase("liquibase-migration/fill-3.sql", new ClassLoaderResourceAccessor(), database);
            picturesFilling.update("");
        } finally {
            connectionProvider.closeConnection(connection);
        }
    }

    public static void createOrUpdateDatabaseSchema() throws Exception {
        if (sessionFactory == null) sessionFactory = createSessionFactory();
        ConnectionProvider connectionProvider = sessionFactory.getSessionFactoryOptions().getServiceRegistry().getService(ConnectionProvider.class);
        Connection connection = connectionProvider.getConnection();
        try {
            Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(connection));

            Liquibase sqlSchema = new Liquibase("liquibase-migration/schema.sql", new ClassLoaderResourceAccessor(), database);
            sqlSchema.update("");

            Liquibase schemaChanging = new Liquibase("liquibase-migration/changelog.xml", new ClassLoaderResourceAccessor(), database);
            schemaChanging.update("");
        } finally {
            connectionProvider.closeConnection(connection);
        }
    }

    private static SessionFactory createSessionFactory() {
        Configuration configuration = createHibernateConfiguration();
        StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
        serviceRegistryBuilder.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
        return configuration.buildSessionFactory(serviceRegistry);
    }

    private static Configuration createHibernateConfiguration() {
        Configuration configuration = new Configuration();
        configuration.setProperty("hibernate.dialect", "org.hibernate.dialect.PostgreSQL9Dialect");
        configuration.setProperty("hibernate.connection.driver_class", "org.postgresql.Driver");

        StringBuilder connectionUrlBuilder = new StringBuilder();
        String host = mainConfig.dbHost();
        String port = mainConfig.dbPort();
        String db = mainConfig.dbName();
        String login = mainConfig.dbUsername();
        String password = mainConfig.dbPassword();

        connectionUrlBuilder
                .append("jdbc:postgresql://")
                .append(host).append(":")
                .append(port).append("/")
                .append(db);

        String connectionUrl = connectionUrlBuilder.toString();
        String jdbcDatabaseUrl = System.getenv().get("JDBC_DATABASE_URL");
        if (jdbcDatabaseUrl != null) {
            logger.info("Found System Env Property 'JDBC_DATABASE_URL' = {}\nSeems that application starts by HEROKU. I will use this property for database connecting", jdbcDatabaseUrl);
            connectionUrl = jdbcDatabaseUrl;
        }

        configuration.setProperty("hibernate.connection.url", connectionUrl);
        configuration.setProperty("hibernate.connection.username", login);
        configuration.setProperty("hibernate.connection.password", password);
        configuration.setProperty("hibernate.connection.pool_size", "4");
        configuration.setProperty("hibernate.temp.use_jdbc_metadata_defaults", "false"); // this speed up hibernate init
        //configuration.setProperty("hibernate.show_sql", "true"); // enable sql in log

        initAnnotatedClasses(configuration);

        return configuration;
    }

    private static void initAnnotatedClasses(Configuration configuration) {
        configuration.addAnnotatedClass(HandledUpdate.class);
        configuration.addAnnotatedClass(Question.class);
        configuration.addAnnotatedClass(Picture.class);
        configuration.addAnnotatedClass(Round.class);
        configuration.addAnnotatedClass(Game.class);
        configuration.addAnnotatedClass(User.class);
    }

}
