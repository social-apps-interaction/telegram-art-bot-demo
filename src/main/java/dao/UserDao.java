package dao;

import dao.entity.User;

import java.util.Optional;

public interface UserDao {

    Optional<User> getUser(Integer id);

    void saveOrUpdate(User user);
}
