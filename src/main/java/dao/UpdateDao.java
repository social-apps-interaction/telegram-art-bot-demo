package dao;

import dao.entity.HandledUpdate;

import java.util.Optional;

/**
 * Created by sobolev on 9/7/2017.
 */
public interface UpdateDao {

    Optional<HandledUpdate> getUpdate(Integer id);

    void save(HandledUpdate updateEntity);

    Integer getLastUpdateId();
}
