package dao.impl;

import dao.GameDao;
import dao.DatabaseManager;
import dao.entity.Game;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Created by Ilya on 22.09.2017.
 */
public class GameDaoImpl implements GameDao {


    @Override
    public void save(Game game) {
        Session session = DatabaseManager.openSession();
        try {
            game.setUpdDate(Instant.now());
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(game);
            transaction.commit();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Game> get(Integer id) {
        Session session = DatabaseManager.openSession();
        try {
            return session.byId(Game.class).loadOptional(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Game> getCurrentGame(Integer userId) {
        Session session = DatabaseManager.openSession();
        try {
            Query<Game> query = session.createQuery("from Game where user_id=:userId and is_finished=false", Game.class);
            query.setParameter("userId", userId);
            return query.uniqueResultOptional();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Game> getUserGames(Integer userId) {
        Session session = DatabaseManager.openSession();
        try {
            Query<Game> userGamesQuery = session.createQuery("from Game where user_id=:userId", Game.class).setParameter("userId", userId);
            List<Game> userGames = userGamesQuery.list();
            return userGames;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public Optional<Game> getLastUserGame(Integer userId) {
        Session session = DatabaseManager.openSession();
        try {
            Query<Game> userGamesQuery = session.createQuery("from Game where user_id=:userId1 order by upd_date desc", Game.class)
                    .setParameter("userId1", userId);
            userGamesQuery.setMaxResults(1);
            return userGamesQuery.uniqueResultOptional();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public List<Integer> getUsersIds() {
        Session session = DatabaseManager.openSession();
        try {
            NativeQuery nativeQuery = session.createNativeQuery("select distinct(user_id) from game;");
            return nativeQuery.list();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }
}
