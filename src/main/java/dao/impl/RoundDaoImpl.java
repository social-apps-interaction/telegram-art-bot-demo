package dao.impl;

import dao.RoundDao;
import dao.DatabaseManager;
import dao.entity.Round;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.Optional;

/**
 * Created by Ilya on 19.09.2017.
 */
public class RoundDaoImpl implements RoundDao {

    @Override
    public Optional<Round> getRound(Integer id) {
        Session session = DatabaseManager.openSession();
        try {
            return session.byId(Round.class).loadOptional(id);
        } finally {
            session.close();
        }
    }

    @Override
    public void saveRound(Round round) {
        if (round.getQuestion().getId() == null)
            throw new RuntimeException("Can't save round without question_id");

        Session session = DatabaseManager.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(round);
            transaction.commit();
        } finally {
            session.close();
        }
    }

}
