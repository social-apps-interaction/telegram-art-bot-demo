package dao.impl;

import dao.DatabaseManager;
import dao.GameDao;
import dao.PictureDao;
import dao.entity.Game;
import dao.entity.Picture;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Ilya on 18.09.2017.
 */
public class PictureDaoImpl implements PictureDao {

    private final Class entity = Picture.class;
    private GameDao gameDao = new GameDaoImpl();

    @Override
    public Optional<Picture> getPicture(Integer id) {
        Session session = DatabaseManager.openSession();
        try {
            return session.byId(entity).loadOptional(id);
        } finally {
            session.close();
        }
    }

    @Override
    public Picture getRandomPicture() {
        Session session = DatabaseManager.openSession();
        try {
            List<Picture> allPictures = session.createQuery(String.format("from %s", entity.getSimpleName())).list();
            if (allPictures.isEmpty()) throw new RuntimeException("No pictures in database!");
            return allPictures.get(new Random().nextInt(allPictures.size()));
        } finally {
            session.close();
        }
    }

    @Override
    public void save(Picture picture) {
        Session session = DatabaseManager.openSession();
        try {
            picture.setCrDate(Instant.now());
            Transaction transaction = session.beginTransaction();
            session.save(picture);
            transaction.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public Picture getRandomNotPlayedPicture(Integer userId) {
        Session session = DatabaseManager.openSession();
        try {
            List<Integer> playedPictureIds = gameDao.getUserGames(userId).stream().map(Game::getPicture).map(Picture::getId).collect(Collectors.toList());
            if (playedPictureIds.isEmpty()) return getRandomPicture();

            Query<Picture> query = session.createQuery("from Picture where id not in (:playedPictureIds)", Picture.class)
                    .setParameter("playedPictureIds", playedPictureIds);
            List<Picture> notPlayedPictures = query.list();
            if (notPlayedPictures.isEmpty())
                return null;
            return notPlayedPictures.get(new Random().nextInt(notPlayedPictures.size()));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Picture> getAllPictures() {
        Session session = DatabaseManager.openSession();
        try {
            return session.createQuery("from Picture", Picture.class).list();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
