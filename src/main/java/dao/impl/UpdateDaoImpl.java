package dao.impl;

import dao.DatabaseManager;
import dao.UpdateDao;
import dao.entity.HandledUpdate;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.Instant;
import java.util.Optional;

/**
 * Created by Ilya on 18.09.2017.
 */
public class UpdateDaoImpl implements UpdateDao {

    @Override
    public Optional<HandledUpdate> getUpdate(Integer id) {
        Session session = DatabaseManager.openSession();
        try {
            return session.byId(HandledUpdate.class).loadOptional(id);
        } finally {
            session.close();
        }
    }

    @Override
    public void save(HandledUpdate updateEntity) {
        Session session = DatabaseManager.openSession();
        try {
            updateEntity.setHandlingDate(Instant.now());
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(updateEntity);
            transaction.commit();
        } finally {
            session.close();
        }
    }

    @Override
    public Integer getLastUpdateId() {
        Session session = DatabaseManager.openSession();
        try {
            return session.createQuery("select max(id) from HandledUpdate", Integer.class).getSingleResult();
        } finally {
            session.close();
        }
    }
}
