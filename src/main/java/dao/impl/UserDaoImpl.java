package dao.impl;

import dao.DatabaseManager;
import dao.UserDao;
import dao.entity.User;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.time.Instant;
import java.util.Optional;

public class UserDaoImpl implements UserDao {

    @Override
    public Optional<User> getUser(Integer id) {
        Session session = DatabaseManager.openSession();
        try {
            return session.byId(User.class).loadOptional(id);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }

    @Override
    public void saveOrUpdate(User user) {
        user.setUpdDate(Instant.now());
        Session session = DatabaseManager.openSession();
        try {
            Transaction transaction = session.beginTransaction();
            session.saveOrUpdate(user);
            transaction.commit();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            session.close();
        }
    }
}
