package dao;

import dao.entity.Picture;

import java.util.List;
import java.util.Optional;

public interface PictureDao {

    Optional<Picture> getPicture(Integer id);

    Picture getRandomPicture();

    void save(Picture picture);

    Picture getRandomNotPlayedPicture(Integer userId);

    List<Picture> getAllPictures();
}
