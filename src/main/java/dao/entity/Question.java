package dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by sobolev on 9/5/2017.
 */
@Entity
@Table(name = "question")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "picture_id", nullable = false)
    private Picture picture;

    @Column(name = "question", length = 1000)
    private String question;

    @Column(name = "answer")
    private String answer;

    @Column(name = "incorrect_answer_1")
    private String incorrectAnswerA;

    @Column(name = "incorrect_answer_2")
    private String incorrectAnswerB;

}
