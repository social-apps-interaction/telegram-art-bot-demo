package dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by sobolev on 9/5/2017.
 */
@Entity
@Table(name = "round")
@Data
@NoArgsConstructor
public class Round {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "question_id", nullable = false)
    private Question question;

    @Column(name = "is_won")
    private Boolean isWon;

    @ManyToOne
    @JoinColumn(name = "game_id", nullable = false)
    private Game game;

    @Column(name = "is_current")
    private Boolean isCurrent;

    @Column(name = "user_answer")
    private String userAnswer;

}
