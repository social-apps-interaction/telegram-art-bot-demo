package dao.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

/**
 * Created by sobolev on 9/5/2017.
 */
@Entity
@Table(name = "picture")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = "questions")
public class Picture {

    public Picture(String fileName, String caption, List<Question> questions, Instant crDate) {
        this.fileName = fileName;
        this.caption = caption;
        this.questions = questions;
        this.crDate = crDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "caption")
    private String caption;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "picture", cascade = CascadeType.ALL) // todo persist remove?
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Question> questions;

    @Column(name = "cr_date")
    private Instant crDate;
}
