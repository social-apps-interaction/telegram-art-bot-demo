package dao.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.Instant;

@Data
@NoArgsConstructor
@Entity
@Table(name = "telegram_user")
public class User {

    @Id
    //@GeneratedValue(strategy = GenerationType.IDENTITY) id should be getted from telegram
    private Integer id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "language_code")
    private String languageCode;

    @Column(name = "state")
    private String state;

    @Column(name = "upd_date")
    private Instant updDate;

    public User(com.pengrad.telegrambot.model.User telegramUser) {
        id = telegramUser.id();
        firstName = telegramUser.firstName();
        lastName = telegramUser.lastName();
        languageCode = telegramUser.languageCode();
    }

}
