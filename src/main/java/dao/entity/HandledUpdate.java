package dao.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

/**
 * Created by sobolev on 9/7/2017.
 */
@Entity
@Table(name = "handled_update")
@Data
public class HandledUpdate {

    @Id
    @Column(name = "update_id")
    private Integer updateId;

    @Column(name = "handling_date")
    private Instant handlingDate;

    @Column(name = "is_handled")
    private Boolean isHandled;

}
