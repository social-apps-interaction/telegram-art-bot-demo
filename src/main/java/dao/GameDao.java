package dao;

import dao.entity.Game;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ilya on 22.09.2017.
 */
public interface GameDao {

    void save(Game game);

    Optional<Game> get(Integer id);

    Optional<Game> getCurrentGame(Integer userId);

    List<Game> getUserGames(Integer userId);

    Optional<Game> getLastUserGame(Integer userId);

    List<Integer> getUsersIds();
}
