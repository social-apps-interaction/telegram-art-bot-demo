package dao;

import dao.entity.Round;

import java.util.Optional;

public interface RoundDao {

    Optional<Round> getRound(Integer id);

    void saveRound(Round round);

}
